/**
 *
 * @author Pepito
 */

import java.util.Random;

public class Principal {
    
    
    Random  ale = new Random();
    private int n = 100, bloque = 2; //Por cada unidad o bit de la senal D hay 2 del reloj, es decir, hay un periodo (un 1 y un 0).
    private int[] signal = new int[n];
    private int[] clock = new int[bloque];  //como cada parte de D representa 2 bloques del reloj (un periodo de 1 y 0).
    private int[] secuenciaM = new int[n*bloque];
    private int[][] q = new int[5][bloque];
    private int[][] d = new int[5][bloque];
    
    
    Principal(){        
        iniciarClock();
        
    }
    
    
    public void generarSignal(){
        for (int i = 0; i < this.signal.length; i++) {
            this.signal[i] = (int)(ale.nextDouble()*2);
            System.out.print(signal[i]+" | ");            
        }
    }
    
    public void secuenciaM_5_1(){
        resetQD();
        resetsecuenciaM();
        //int xor1=0, xor2=0;
        
        for (int l = 0; l < n*bloque; l++) {   //el ciclo supuestamente se debería repetir cada 31 valores por lo que se haya.
            for (int i = 0; i < this.q.length; i++) {
            
                //XOR
                if (i==0) {
                    for (int j = 0; j < this.bloque; j++) {
                        d[i][j]=xor(q[0][j], q[4][j]);   //traigo el valor del último Q y del Q1 para hacer el XOR y convertirlo en entrada D.
                    }
                }
                
                
                for (int j = 0; j < this.bloque; j++) {
                    if (d[i][j]==1) {
                        if (clock[j]==1) {
                            if (i==0 && j==0) {
                                q[0][0]=1;  //el caso en que el primer valor pida dar como Q el Q anterior, pero como es el primero se igual.
                            }else{
                                if (j==0) {
                                    q[i][j]=q[i-1][bloque];
                                }else{
                                    q[i][j]=q[i][j-1];
                                }                                
                            }                            
                        }else{
                            q[i][j]=1;
                        }
                    }else if (clock[j]==1) {
                            if (i==0 && j==0) {
                                q[0][0]=1;  //el caso en que el primer valor pida dar como Q el Q anterior, pero como es el primero se igual.
                            }else{
                                if (j==0) {
                                    q[i][j]=q[i-1][bloque];
                                }else{
                                    q[i][j]=q[i][j-1];
                                }                                
                            }  
                    }else{
                        q[i][j]=0;
                    }                    
                }
                
                
                //asignación del resultado en el vector saliente.                
                if (i==4) {
                    secuenciaM[l]=q[i][bloque-1];
                }
            }
        }
        imprimirRTA();
    }
    
    public void imprimirRTA(){
        for (int i = 0; i < secuenciaM.length; i++) {
            System.out.print(secuenciaM[i]+" |");
        }
    }     
    
    
    public void resetQD(){
        for (int i = 0; i < this.q.length; i++) {            
            for (int j = 0; j < this.bloque; j++) {
                this.q[i][j]=1;
                this.d[i][j]=0;
            }            
        }
    }
    
    public void iniciarClock(){
        for (int i = 0; i < this.clock.length; i++) {
            this.clock[i]= i % 2;
        }
    }
    
    public void resetsecuenciaM(){
        for (int i = 0; i < this.secuenciaM.length; i++) {
            this.secuenciaM[i]=0;
        }
    }
    
    public int xor(int x, int y){
        if (x+y == 1){
		return 1;
        }else{
		return 0;
        }
    }
            
    
    
    
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.secuenciaM_5_1();
        
    }
    
}
