import com.panamahitek.PanamaHitek_Arduino;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class VentanaUltrasonido extends javax.swing.JFrame {

    Ultrasonido ultrasonido;
    boolean unico = false;
    
    //Imagenes por defecto
    ImageIcon imgCam = new ImageIcon("src/imgs/cam.png");
    ImageIcon imgDiamante = new ImageIcon("src/imgs/diamante.png");
    ImageIcon imgOpcional = new ImageIcon("");
    ImageIcon imgPolicia = new ImageIcon("src/imgs/policia.gif");
    ImageIcon imgPoli = new ImageIcon("src/imgs/poli.gif");
    ImageIcon imgPoli2 = new ImageIcon("src/imgs/poli2.jpg");
    ImageIcon imgSecure = new ImageIcon("src/imgs/secure.jpg");
    ImageIcon imgAlarma = new ImageIcon("src/imgs/alarma.gif");
    ImageIcon imgAlerta = new ImageIcon("src/imgs/alerta.gif");
    ImageIcon imgLadron1 = new ImageIcon("src/imgs/ladron1.gif");
    ImageIcon imgLadron2 = new ImageIcon("src/imgs/ladron2.gif");
    ImageIcon imgLupa = new ImageIcon("src/imgs/lupa.jpg");
    ImageIcon imgSonido = new ImageIcon("src/imgs/sonido.gif");
    ImageIcon imgAlerta2 = new ImageIcon("src/imgs/alerta2.jpg");
    
    String nombreSonido = "src/sonidos/alert3.wav";
    AudioInputStream audioInputStream;
    Clip clip;
    
    public VentanaUltrasonido() {
        
        resetearAudio();
        
        initComponents();
        this.setLocationRelativeTo(null);
        ultrasonido = new Ultrasonido(this);
        ultrasonido.start();
    }
    
    public void resetearAudio(){
        try {
            this.audioInputStream = AudioSystem.getAudioInputStream(new File(nombreSonido).getAbsoluteFile());
            this.clip = AudioSystem.getClip();             
            this.clip.open(audioInputStream);
                          
        } catch(UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
            System.out.println("Error al reproducir el sonido.");
        }
    }

    public void cambiarDatos (int distancia){
        
        //System.out.println("Distancia: "+distancia);
                       
        if(distancia>=10 && distancia<=12){    //TD BN.
            
            Icon iconoImgCam = new ImageIcon(imgCam.getImage().getScaledInstance(labelImgCam.getWidth(),labelImgCam.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgDiamante = new ImageIcon(imgDiamante.getImage().getScaledInstance(labelImgDiamante.getWidth(),labelImgDiamante.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgOpcional = new ImageIcon(imgOpcional.getImage().getScaledInstance(labelImgOpcional.getWidth(),labelImgOpcional.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgPolicia = new ImageIcon(imgPolicia.getImage().getScaledInstance(labelImgPolicia.getWidth(),labelImgPolicia.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgSecure = new ImageIcon(imgSecure.getImage().getScaledInstance(labelImgSecure.getWidth(),labelImgSecure.getHeight(),Image.SCALE_DEFAULT));
            
            labelImgCam.setIcon(iconoImgCam);
            labelImgDiamante.setIcon(iconoImgDiamante);
            labelImgOpcional.setIcon(iconoImgOpcional);
            labelImgPolicia.setIcon(iconoImgPolicia);
            labelImgSecure.setIcon(iconoImgSecure);
            
            //this.repaint();   
            //alarma(0);
            this.clip.close();
            this.unico = false;
            //labelLuminosidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/luna.gif")));
        }
        else if (distancia>=8 && distancia<=14){  //ALERTA
            Icon iconoImgCam = new ImageIcon(imgCam.getImage().getScaledInstance(labelImgCam.getWidth(),labelImgCam.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgDiamante = new ImageIcon(imgDiamante.getImage().getScaledInstance(labelImgDiamante.getWidth(),labelImgDiamante.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgOpcional = new ImageIcon(imgAlerta2.getImage().getScaledInstance(labelImgOpcional.getWidth(),labelImgOpcional.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgPolicia = new ImageIcon(imgPoli2.getImage().getScaledInstance(labelImgPolicia.getWidth(),labelImgPolicia.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgSecure = new ImageIcon(imgLupa.getImage().getScaledInstance(labelImgSecure.getWidth(),labelImgSecure.getHeight(),Image.SCALE_DEFAULT));
            
            labelImgCam.setIcon(iconoImgCam);
            labelImgDiamante.setIcon(iconoImgDiamante);
            labelImgOpcional.setIcon(iconoImgOpcional);
            labelImgPolicia.setIcon(iconoImgPolicia);
            labelImgSecure.setIcon(iconoImgSecure);
            
            //alarma(0);
            this.clip.close();
            this.unico = false;
            //this.repaint();
            //labelLuminosidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nube.png")));
        }else{    //ROBO!
            Icon iconoImgCam = new ImageIcon(imgCam.getImage().getScaledInstance(labelImgCam.getWidth(),labelImgCam.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgDiamante = new ImageIcon(imgAlerta.getImage().getScaledInstance(labelImgDiamante.getWidth(),labelImgDiamante.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgOpcional = new ImageIcon(imgLadron2.getImage().getScaledInstance(labelImgOpcional.getWidth(),labelImgOpcional.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgPolicia = new ImageIcon(imgPoli.getImage().getScaledInstance(labelImgPolicia.getWidth(),labelImgPolicia.getHeight(),Image.SCALE_DEFAULT));
            Icon iconoImgSecure = new ImageIcon(imgSonido.getImage().getScaledInstance(labelImgSecure.getWidth(),labelImgSecure.getHeight(),Image.SCALE_DEFAULT));
            
            labelImgCam.setIcon(iconoImgCam);
            labelImgDiamante.setIcon(iconoImgDiamante);
            labelImgOpcional.setIcon(iconoImgOpcional);
            labelImgPolicia.setIcon(iconoImgPolicia);
            labelImgSecure.setIcon(iconoImgSecure);
            //labelImgDiamante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/alerta.gif")));
            if (!this.unico) {
                resetearAudio();
                this.clip.start();
                this.clip.loop(Clip.LOOP_CONTINUOUSLY);
                //alarma(1);
                this.unico = true;
            }
            
            
            
        }        
        
    }
    
    
    public void alarma(int tipo){
        if (tipo == 1) {
            this.clip.start();
            this.clip.loop(Clip.LOOP_CONTINUOUSLY);
        }else{
            this.clip.close();
        }              
        
    } 
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelTitulo = new javax.swing.JLabel();
        labelImgDiamante = new javax.swing.JLabel();
        labelImgPolicia = new javax.swing.JLabel();
        labelImgSecure = new javax.swing.JLabel();
        labelImgCam = new javax.swing.JLabel();
        labelImgOpcional = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("UltrasonidoXBee");

        labelTitulo.setBackground(new java.awt.Color(255, 204, 102));
        labelTitulo.setFont(new java.awt.Font("Verdana", 0, 60)); // NOI18N
        labelTitulo.setForeground(new java.awt.Color(255, 0, 0));
        labelTitulo.setText("ULTRASEGURIDAD");

        labelImgDiamante.setHorizontalAlignment(labelImgDiamante.getHorizontalAlignment());
        labelImgDiamante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/diamante.png"))); // NOI18N

        labelImgPolicia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/policia.gif"))); // NOI18N

        labelImgSecure.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/secure.jpg"))); // NOI18N

        labelImgCam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/cam.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(labelImgSecure, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(labelTitulo)
                        .addGap(28, 28, 28))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(labelImgPolicia, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(labelImgDiamante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelImgCam, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelImgOpcional, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(labelImgCam, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelImgOpcional, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(labelImgSecure, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(47, 47, 47)
                                .addComponent(labelTitulo)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(237, 237, 237)
                                .addComponent(labelImgPolicia, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(labelImgDiamante, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaUltrasonido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaUltrasonido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaUltrasonido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaUltrasonido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaUltrasonido().setVisible(true);
            }
        });
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel labelImgCam;
    private javax.swing.JLabel labelImgDiamante;
    private javax.swing.JLabel labelImgOpcional;
    private javax.swing.JLabel labelImgPolicia;
    private javax.swing.JLabel labelImgSecure;
    private javax.swing.JLabel labelTitulo;
    // End of variables declaration//GEN-END:variables



}


class Ultrasonido extends Thread{
    
    VentanaUltrasonido ventana;
    PanamaHitek_Arduino arduino = new PanamaHitek_Arduino();
    public String puerto = "COM3";
    SerialPortEventListener evento = new SerialPortEventListener() {

        @Override
        public void serialEvent(SerialPortEvent spe) {
            if(arduino.isMessageAvailable()== true){
                int distancia = Integer.parseInt(arduino.printMessage());
                ventana.cambiarDatos(distancia);
            }
        }
    };
    
    public Ultrasonido (VentanaUltrasonido ventana){
        this.ventana = ventana;
    }
    
    @Override
    public void run(){
        try {
            arduino.arduinoRXTX(puerto, 9600, evento);
        } catch (Exception ex) {
            Logger.getLogger(VentanaUltrasonido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}