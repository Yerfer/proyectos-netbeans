
import java.util.Scanner;
//import java.math.BigInteger;

public class Principal {
    
private int nCasos = 0,base=1;
private String caso = "";
private char[] casoCadena;
private int[] valores;
Scanner leer = new Scanner(System.in);
//BigInteger rtaAcu = new BigInteger("0");
java.math.BigInteger rtaAcu = new java.math.BigInteger("0"); 

public void pedirN(){
    this.nCasos = leer.nextInt();
    for (int i = 0; i < this.nCasos; i++) {
        leerCaso();
        System.out.println("Case #"+(i+1)+": "+this.rtaAcu.toString());
    }
}

public void leerCaso(){
    this.caso = leer.next();
    procesar();
}

public void procesar(){
    this.casoCadena = this.caso.toCharArray();
    this.valores = new int[this.casoCadena.length];
    this.valores[0]=1;    //Por las restricciones de ejercicio.
    for (int i = 1; i < this.casoCadena.length; i++) {
        if(!buscar(this.casoCadena[i],i)){
            //asigna en la posición actual el valor repetido.
            System.out.println("repe");
        }
        else{
            if(i==1){
                this.valores[i] = 0;
                System.out.println("dife");
            }
            else{
                this.valores[i] = i;
                System.out.println("dife1");
            }            
            this.base++;
        }
    }
    operar();
}

public void operar(){    
    for (int i = 0; i < this.casoCadena.length; i++) {
        System.out.print(this.casoCadena[i]+"|");
    }
    System.out.println("");
    for (int i = 0; i < this.valores.length; i++) {
        System.out.print(this.valores[i]+"|");
    }
    System.out.println("");
    int cont = 0;
    rtaAcu = java.math.BigInteger.valueOf(0);
    for (int i = (this.valores.length-1); i >= 0; i--) {
        int temporal = this.valores[i] * (int) Math.pow(this.base, cont);
        rtaAcu = rtaAcu.add(java.math.BigInteger.valueOf(temporal));
        cont++;
    }    
}


public boolean buscar(char simbolo, int posActual){
    for (int j = 0; j < posActual; j++) {
        if(this.casoCadena[j]==simbolo){
            this.valores[posActual] = this.valores[j];
            return true;
        }
    }
    return false;
}



    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.pedirN();
    }




}
