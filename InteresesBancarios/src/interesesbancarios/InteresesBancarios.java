package interesesbancarios;

import java.util.Random;

public class InteresesBancarios {
    
    private int n;
    private int m;

    public InteresesBancarios(int n, int m) {
        this.n = n;
        this.m = m;
    }
        
    public int intereses(int[][] f, int[][] I){
        for (int i = 1; i <= this.n; i++) {
            I[i-1][0] = 0;      //Valores Iniciales.            
        }
        for (int j = 1; j <= this.m; j++) {
            I[1][j-1] = f[1][j-1];
        }
        for (int i = 2; i <= this.n; i++) {
            for (int j = 1; j <= this.m; j++) {
                I[i-1][j-1] = maximo(I,f,i-1,j-1);
            }
        }
        return I[this.n-1][this.m-1];
    }
    
    public int maximo(int[][] I, int[][] f, int i, int j){
        int max;
        max = I[i-1][j] + f[i][0];
        for (int t = 1; t <= j; t++) {
            max = Math.max(max, I[i-1][j-t] + f[i][t]);
        }
        return max;
    }

    
    public static void main(String[] args) {
        Random rnd = new Random();
        int n = 4;  // # de bancos.
        int m = 10;  // $$ de pesetas.
        int[][] f = new int[n][m];
        int[][] I = new int[n][m];
        int[] intereses = {2,3,4,5,6};
        System.out.println("F[i,j] representa el interés del banco i para j pesetas.");
        System.out.print("\t");
        for (int j = 1; j <= m; j++) {
            System.out.print("*$"+j+"*\t");
        }
        for (int i = 0; i < n; i++) {
            System.out.print("\ni = "+(i+1)+"\t");
            for (int j = 0; j < m; j++) {
                int x = rnd.nextInt(5);
                f[i][j] = intereses[x];
                System.out.print("| "+f[i][j]+" |\t");                
            }            
        }
                
        InteresesBancarios obj1 = new InteresesBancarios(n, m); 
        System.out.print("\nLa respuesta es: ");
        System.out.println(obj1.intereses(f, I));
    }
    
}
