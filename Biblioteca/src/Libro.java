/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Yerson Porras
 */
public class Libro {
    
    private int codLibro, codReserva;
    private String autor, titulo, tema;

    public Libro(int codLibro, int codReserva, String autor, String titulo, String tema) {
        this.codLibro = codLibro;
        this.codReserva = codReserva;
        this.autor = autor;
        this.titulo = titulo;
        this.tema = tema;
    }
    
    public Libro() {
        this.codLibro = 1234;
        this.codReserva = 0;
        this.autor = "Gabriel Garcia Marquez";
        this.titulo = "Cien Años de Soledad";
        this.tema = "Literatura";
    }
    
    public Libro(int codReserva) {
        this.codLibro = 1234;
        this.codReserva = codReserva;
        this.autor = "Gabriel Garcia Marquez";
        this.titulo = "Cien Años de Soledad";
        this.tema = "Literatura";
    }
    
    public void prestarLibro(int codCliente, int codLibro){
        if (0==codReserva) {
            System.out.println("Se registró el préstamo del libro "+codLibro+" al cliente "+codCliente);
        }
        else{
            if(codCliente==codReserva){
                System.out.println("Se registró el préstamo del libro "+codLibro+" con reserva "+this.codReserva+" correspondiente al cliente "+codCliente);
            }
            else{
                System.out.println("NO se registró el préstamo del libro "+codLibro+" con reserva "+this.codReserva+" correspondiente al cliente "+codCliente);

            }
        }
    }
    
}
