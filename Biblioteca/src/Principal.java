/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luz Mery
 */
public class Principal {
    
    public static void main(String[] args) {
        int codCLiente = 1111, codLibro = 1234;
        Libro libro1 = new Libro();
        libro1.prestarLibro(codCLiente, codLibro);
        Libro libro2 = new Libro(1111); //Enviando el codigo de la reserva al constructor para verificar con los datos internos.
    }
}
