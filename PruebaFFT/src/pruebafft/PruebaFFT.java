package pruebafft;

/**
 *
 * @author Yerson Porras
 */


//import java.lang.Object;
import org.apache.commons.math3.transform.*;
import org.apache.commons.math3.transform.DftNormalization;
//import org.apache.commons.math3.*;  
//import static org.apache.commons.math3.transform.DftNormalization.STANDARD;

public class PruebaFFT {

    //public static final DftNormalization STANDARD;
    
    
    
    public static void main(String[] args) {
        //public static final DftNormalization STANDARD; XXXXX
        //DftNormalization norma = STANDARD;  XXXXXXXX
        int n = 8;  //Tiene que ser potencia de 2;
        double[] vector = new double[n];
        int[] salida = new int[n];
        for (int i = 0; i < vector.length; i++) {
            //vector[i] = (int) (Math.random()*2);
            vector[i] = (int) (i*2);
        }
        FastFourierTransformer obj = new FastFourierTransformer(DftNormalization.STANDARD);
        
        System.out.println("Vector:");
        for (int i = 0; i < vector.length; i++) {
            System.out.print(vector[i] +" | ");
        }
        System.out.println("\nFFT:");
        for (int i = 0; i < vector.length; i++) {
            System.out.print(obj.transform(vector, TransformType.FORWARD)[i].getReal() +" | ");
        }
        
    }
    
}
