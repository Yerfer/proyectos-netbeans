/**
 *
 * @author Yerson Porras
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Principal {
    
    Scanner leer = new Scanner(System.in);
    int nCasos;
    String cadena;
    ArrayList <Character> zero = new ArrayList<Character>(); 
    ArrayList <Character> one = new ArrayList<Character>();
    ArrayList <Character> two = new ArrayList<Character>();
    ArrayList <Character> three = new ArrayList<Character>();
    ArrayList <Character> four = new ArrayList<Character>();
    ArrayList <Character> five = new ArrayList<Character>();
    ArrayList <Character> six = new ArrayList<Character>();
    ArrayList <Character> seven = new ArrayList<Character>();
    ArrayList <Character> eight = new ArrayList<Character>();
    ArrayList <Character> nine = new ArrayList<Character>();
    ArrayList[] digitos = {zero,one,two,three,four,five,six,seven,eight,nine};
    
    public void llenar(){
        zero.add('Z');zero.add('E');zero.add('R');zero.add('O');
        one.add('O');one.add('N');one.add('E');
        two.add('T');two.add('W');two.add('O');
        three.add('T');three.add('H');three.add('R');three.add('E');three.add('E');
        four.add('F');four.add('O');four.add('U');four.add('R');
        five.add('F');five.add('I');five.add('V');five.add('E');
        six.add('S');six.add('I');six.add('X');
        seven.add('S');seven.add('E');seven.add('V');seven.add('E');seven.add('N');
        eight.add('E');eight.add('I');eight.add('G');eight.add('H');eight.add('T');
        nine.add('N');nine.add('I');nine.add('N');nine.add('E');
    }
    
    
    public void inicia(){
        llenar();
        nCasos = leer.nextInt();
        for (int i = 0; i < nCasos; i++) {
            cadena=leer.next();
            ArrayList rtas = new ArrayList();
            System.out.print("Case #"+(i+1)+": ");
            operar(cadena,i+1,0, rtas);
            System.out.println("");
        }        
    }
    
    public void operar(String cadenaS,int casoN,int inicio, ArrayList rtas){
        //System.out.println("CadenaS:  "+cadenaS);
        char[] cadena = cadenaS.toCharArray();
        boolean rta=false;
        ArrayList cadenaLista = new ArrayList();
        
        for (int i = 0; i < cadena.length; i++) {            
                cadenaLista.add(cadena[i]);                   
        }
        
        int i=inicio;        
        do{
            //for (int i = inicio; i < digitos.length; i++) {
                System.out.println("CadenaLista: "+cadenaLista);
                System.out.println("Digito: "+i);
                
                ArrayList cadenaListaTemp = (ArrayList) cadenaLista.clone();
                for (int j = 0; j < digitos[i].size(); j++) {
                    
                    System.out.println("al entrar al for i= "+i);
                    System.out.println("Letra: "+digitos[i].get(j));
                    
                    if(cadenaListaTemp.remove(digitos[i].get(j))){
                        
                        System.out.println("Existe!");
                        
                        if(j==digitos[i].size()-1){              
                            
                            System.out.println("Es la ultima");                            
                            cadenaLista = (ArrayList) cadenaListaTemp.clone();
                            rtas.add(i);
                            //i=0; 
                            i=inicio;
                            System.out.println("debería reiniciar i= "+i);
                            break;
                        }                        
                    }else{
                        i++;
                        break;
                    }
                }            
            //}
                //i++;
                System.out.println("i++: "+i);
        } 
        while(i < digitos.length);
        
        
        if (!cadenaLista.isEmpty()) {
                rtas.clear();
                System.out.println("VUELVE Y JUEGA!!!!!!!!!!!!!!!!!!!!!!");
                operar(cadenaS,casoN,inicio+1,rtas);
        } 
        else{
            Collections.sort(rtas);
            for (int k = 0; k < rtas.size(); k++) {
                System.out.print(rtas.get(k));
            }            
        }
    }
    
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.inicia();
    }

}
