package Principal;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
    
    private static Connection conexion;
    //Statement sen;
    //ResultSet res;
    private  static String usuario;
    private  static String clave;
    private  static String nameDB = "XE";
    private  static String baseDeDatos = "jdbc:oracle:thin:@localhost:1521:"+nameDB; 

    public Conexion(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
        this.baseDeDatos = "jdbc:oracle:thin:@localhost:1521:"+this.nameDB;
        this.nameDB = "XE";
    }

    public static Connection getConexion() {
        return conexion;
    }

    public static void setConexion(Connection conexion2) {
        conexion = conexion2;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        usuario = usuario;
    }

    public static String getClave() {
        return clave;
    }

    public static void setClave(String clave2) {
        clave = clave2;
    }
    
    public static String getNameDB() {
        return nameDB;
    }

    public static void setNameDB(String nameDB2) {
        nameDB = nameDB2;
    }

    public static String getBaseDeDatos() {
        return baseDeDatos;
    }

    public static void setBaseDeDatos(String baseDeDatos2) {
        baseDeDatos = baseDeDatos2;
    }
    
    
    public static boolean crearConexionDB(String usuario1, String clave1, String cadConexion1){   
        String baseDatos = "jdbc:oracle:thin:@localhost:1521:"+cadConexion1;
        try {
            Class.forName("oracle.jdbc.OracleDriver");         
            conexion = DriverManager.getConnection(baseDatos, usuario1, clave1);
            if (conexion != null) {
                usuario=usuario1;
                clave=clave1;
                nameDB=cadConexion1;
                return true;
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;   
    }
    
    public static ResultSet getPrimaryKey(String tabla) throws SQLException{
        ResultSet rs = null;
        DatabaseMetaData meta = conexion.getMetaData();
        rs = meta.getPrimaryKeys(null, null, tabla);
        return rs;
    }
    
    public static boolean cerrarConexionDB() throws SQLException{
        if (conexion != null) {
                conexion.close();
                usuario=null;
                clave=null;
                nameDB=null;
                conexion=null;
                return true;
            } else {
            }
        return false;
    }
    
    public static boolean existeConexionDB(){
        if (conexion != null) {
                return true;
            } else {
            }
        return false;
    }
    
    public static ResultSet consultaDB(String consulta){
        Statement sentencia;
        ResultSet resultado = null;
        try {
            sentencia=conexion.createStatement();
            resultado=sentencia.executeQuery(consulta);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultado;
    }
    
    public static boolean actualizar(String consulta){
        Statement sentencia;
        try {
            sentencia=conexion.createStatement();
            sentencia.executeUpdate(consulta); 
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);            
            //commit();
            return false;
        }
        commit();
        return true;
    }
    
    public static boolean ejecutar(String consulta){
        Statement sentencia;
        try {
            sentencia=conexion.createStatement();
            sentencia.execute(consulta);
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);            
            //commit();
            return false;
        }
        commit();
        return true;
    }
    
    public static String ejecutarFuncion1(String consulta){
        String respuesta="";
        Statement sentencia;
        try {
            sentencia=conexion.createStatement();
            ResultSet rta = sentencia.executeQuery(consulta);
            if(rta.next()){
                respuesta=rta.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);            
            //commit();
            return "false";
        }
        commit();
        return respuesta;
    }
    
    public static void commit(){
        Statement sentencia;
        try {
            sentencia=conexion.createStatement();
            sentencia.executeQuery("commit");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    
}
