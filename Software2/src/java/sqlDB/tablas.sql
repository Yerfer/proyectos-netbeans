CREATE TABLE libro(
libro NUMBER(5) PRIMARY KEY,
titulo VARCHAR2(250) NOT NULL,
autor VARCHAR2(250),
numpag NUMBER(4)
); 

INSERT INTO libro (libro, titulo, autor, numpag) VALUES (1, 'La Leccion', 'E.Ionesco', 155);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (2, 'El libro de la reposteria', 'Angela Landa', 166);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (3, 'Hamlet', 'Shakespeare', 150);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (4, 'Discurso del metodo', 'R.Descartes', 182);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (5, 'El libro de arena', 'J.L.Borges', 121);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (6, 'Una dama en apuros', 'T. Sharpe', 255);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (7, 'La Republica o el Estado', 'Platon', 303);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (8, 'Job', 'J.Roth', 189);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (9, 'El viejo y el mar', 'E.Hemingwey', 154);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (10, 'En las profundidades', 'A.C.Clarke', 196);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (11, 'Sistemas Operativos', 'E.He---', 100);
INSERT INTO libro (libro, titulo, autor, numpag) VALUES (12, 'Admiministración de Bases de Datos', 'Alarke', 106);


CREATE TABLE socio(
socio NUMBER(5) PRIMARY KEY,
nombre VARCHAR2(250) NOT NULL,
telefono VARCHAR2(250),
ingreso DATE); 

INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (1, 'Juan Perez Lozano', '617315863', to_date('26/03/2007','dd/mm/yyyy'));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (2, 'Luis Romero Sanchez','959253987', to_date('26/03/2007','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (3, 'Pedro Lopez Rojas', '954553153', to_date('26/03/2007','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (4, 'Santiago Gonzalez Lleida', '954435993',to_date('12/06/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (5, 'Fernando Domingo Gomez', '955634281', to_date('26/06/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (6, 'Luis Daza Zamora', '954435113', to_date('26/01/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (7, 'Antonio Ossorio Melgar', '609257167', to_date('26/01/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (8, 'Javier Romero Gonzalez', '617583100', to_date('26/02/2007','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (9, 'Pedro Luis Morales', '617583100', to_date('26/04/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (10, 'Julio Gonzalez', '617583100', to_date('26/09/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (11, 'Javier Gonzalez', '617583100', to_date('26/09/2008','dd/mm/yyyy'  ));
INSERT INTO socio (socio, nombre, telefono, ingreso) VALUES (12, 'Juan Andres Roa', '617583100', to_date('26/09/2008','dd/mm/yyyy'  ));

CREATE TABLE prestamo(
libro NUMBER(5) NOT NULL,
socio NUMBER(5) NOT NULL,
fprestamo DATE NOT NULL,
fdevolucion DATE,
PRIMARY KEY (libro,socio,fprestamo),
FOREIGN KEY (libro) REFERENCES libro,
FOREIGN KEY (socio) REFERENCES socio);

INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (8, 8, to_date('26/01/2009','dd/mm/yyyy'), to_date('26/01/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (4, 8, to_date('26/01/2009','dd/mm/yyyy'), to_date('30/01/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (4, 1, to_date('2/02/2009','dd/mm/yyyy'), to_date('6/02/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (1, 2, to_date('10/02/2009','dd/mm/yyyy'), to_date('10/02/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (8, 3, to_date('10/02/2009','dd/mm/yyyy'), to_date('12/02/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (5, 3, to_date('3/03/2009','dd/mm/yyyy'), to_date('20/03/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (5, 3, to_date('5/03/2009','dd/mm/yyyy'), to_date('20/03/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (5, 3, to_date('2/03/2009','dd/mm/yyyy'), to_date('26/03/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (4, 7, to_date('10/03/2009','dd/mm/yyyy'), to_date('11/09/2009','dd/mm/yyyy'  ));
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (4, 7, to_date('28/02/2009','dd/mm/yyyy'), null);
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (1, 7, to_date('1/03/2009','dd/mm/yyyy'), null);
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (3, 2, to_date('2/04/2009','dd/mm/yyyy'), null);
INSERT INTO prestamo (libro, socio, fprestamo, fdevolucion) VALUES (5, 2, to_date('23/04/2009','dd/mm/yyyy'), null);

commit;
