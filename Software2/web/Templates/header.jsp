<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand">BIENVENIDO!</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <% 
            if (Conexion.existeConexionDB()==true) { 
                   %>
                        <form class="navbar-form navbar-left" METHOD="POST" ACTION="Index.jsp">
                            <div class="input-group">
                                <input name="tabla" type="text" class="form-control" placeholder="Buscar Tabla">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <li><a><span class="glyphicon glyphicon-user"></span> <%= Conexion.getUsuario() %></a></li>
                        <li><a href="Logout.jsp"><span class="glyphicon glyphicon-log-in"></span> Cerrar Sesión</a></li>
                    <% 
                } 
            else{
                %>
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <%
            }
            %>
        </ul>
    </div>
</nav>      
<%@page import="java.sql.*"%>
<%@page import="Principal.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 