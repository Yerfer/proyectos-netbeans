<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href="Templates/signin.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>INDEX</title>
    </head>
    
    
    <body BGCOLOR="#ffffcc">
        <jsp:include page="Templates/header.jsp"></jsp:include>              
        <div class="container-fluid text-center">
            <div class="row content">
                <jsp:include page="Templates/left.jsp"></jsp:include>  
                <% 
                    if (Conexion.existeConexionDB()==false) { 
                        response.sendRedirect("LoginDB.jsp");   
                    }
                %> 
                <div class="col-sm-10 text-left">                       
                    <%
                        if (request.getParameter("tabla")==null){
                        %>

                        <%
                        }else{
                            %>

                            <%
                            String consultaSQL = "Select * from "+request.getParameter("tabla").toUpperCase()+"";
                            ResultSet resultado = Conexion.consultaDB(consultaSQL);
                            if(resultado==null){
                                response.sendRedirect("Index.jsp");
                            }else{
                            %> 
                            <div class="block-" style="margin: 0 auto 0; max-width: 10%;">
                                <h2><%= request.getParameter("tabla").toUpperCase() %></h2>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped"> <% 
                                int nColumnas = resultado.getMetaData().getColumnCount();
                                %>
                                
                                <thead>
                                    <tr>
                                        <%
                                        for (int i = 1; i <= nColumnas; i++) {
                                            %>
                                                <th><%= resultado.getMetaData().getColumnLabel(i) %></th>                             
                                            <%
                                        }
                                        %>
                                            <th>                                                
                                                <form action="Agregar.jsp" method="get">
                                                    <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                    <input href="Agregar.jsp" class="btn btn-success btn-xs" type="submit" value="Agregar">
                                                </form>
                                            </th>
                                    </tr>
                                </thead>
                                <% 
                                    while(resultado.next()){                             
                                        %><tr><%
                                        for (int i = 1; i <= nColumnas; i++) {
                                            %>
                                                <td><%=resultado.getString(i)%></td>                                             
                                            <%
                                        }
                                        %>
                                        <td>
                                            <div class="btn-group btn-group-xs">
                                                
                                                    <%
                                                        if(request.getParameter("tabla").equalsIgnoreCase("PRESTAMO")){
                                                            %>
                                                            <form action="EditarPrestamo.jsp" method="get">
                                                                <input type="hidden" name="libro" value="<%= resultado.getString(1) %>" />
                                                                <input type="hidden" name="socio" value="<%= resultado.getString(2) %>" />
                                                                <input type="hidden" name="fprestamo" value="<%= resultado.getString(3) %>" />
                                                                <input type="hidden" name="fdevolucion" value="<%= resultado.getString(4) %>" />
                                                                <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                                <input class="btn btn-warning btn-xs" type="submit" value="Editar">
                                                            </form> 
                                                            <%
                                                        }
                                                        else{
                                                            %>
                                                            <form action="Editar.jsp" method="get">
                                                                <input type="hidden" name="id" value="<%= resultado.getString(1) %>" />
                                                                <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                                <input class="btn btn-warning btn-xs" type="submit" value="Editar">
                                                            </form> 
                                                            <%
                                                        }
                                                    %>
                                                                                                   
                                                <form action="Eliminar.jsp" method="get">
                                                    <input type="hidden" name="id" value="<%= resultado.getString(1) %>" />
                                                    <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                    <button type="button" class="btn-xs btn-danger">Eliminar</button>
                                                </form>
                                            </div>
                                        </td>
                                        </tr> 

                                        <%

                                    }
                                %> 
                                </table> 
                            </div>
                            <%
                            }
                        }
                    %>                    
                </div>
                <jsp:include page="Templates/right.jsp"></jsp:include>
            </div>
        </div>            
        <jsp:include page="Templates/footer.jsp"></jsp:include>        
    </body>        
    
</html>
