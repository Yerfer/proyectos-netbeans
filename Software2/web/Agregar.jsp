<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <title>Agregar</title>
    </head>
    
    
    <body BGCOLOR="#ffffcc">
        <jsp:include page="Templates/header.jsp"></jsp:include>              
        <div class="container-fluid text-center">
            <div class="row content">
                <jsp:include page="Templates/left.jsp"></jsp:include>  
                <% 
                    if (Conexion.existeConexionDB()==false) { 
                        response.sendRedirect("LoginDB.jsp");   
                    }
                %> 
                <div class="col-sm-10 text-left">                       
                    <%
                        if (request.getParameter("tabla")==null){
                            %>

                            <%
                        }
                        else{
                            if(request.getParameter("tabla").equalsIgnoreCase("PRESTAMO")){
                                response.sendRedirect("AgregarPrestamo.jsp?tabla=Prestamo");  
                            }
                            ResultSet resultado = Conexion.consultaDB("Select * from "+request.getParameter("tabla"));
                            if(resultado==null){
                                response.sendRedirect("Index.jsp");
                            }else{
                                if(request.getParameter(resultado.getMetaData().getColumnLabel(1))==null){
                                    %> 
                                        <div class="" style="margin: 0 auto 0; max-width: 10%;">
                                            <h2><%= request.getParameter("tabla").toUpperCase() %></h2>
                                        </div>
                                        <div class="col-sm-10  block-center">
                                            <form method="post" action="Agregar.jsp">
                                                <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                <% 

                                                int nullable = 0;
                                                int nColumnas = resultado.getMetaData().getColumnCount();
                                                for (int i = 1; i <= nColumnas; i++) {
                                                    String tipoColumna = resultado.getMetaData().getColumnTypeName(i);
                                                    String nombreColumna = resultado.getMetaData().getColumnLabel(i);
                                                    nullable = resultado.getMetaData().isNullable(i);
                                                    %>
                                                    <div class="input-group">
                                                        <span for="<%= nombreColumna %>" class="input-group-addon"><%= nombreColumna %></span>
                                                        <%
                                                            if(nullable==ResultSetMetaData.columnNoNulls){
                                                                %>
                                                                    <input id="<%= nombreColumna %>" required type="<%= tipoColumna.toLowerCase() %>" class="form-control" name="<%= nombreColumna %>" placeholder="<%= nombreColumna %>">
                                                                <%
                                                            }else{
                                                                %>
                                                                    <input id="<%= nombreColumna %>" type="<%= tipoColumna.toLowerCase() %>" class="form-control" name="<%= nombreColumna %>" placeholder="<%= nombreColumna %>">
                                                                <%
                                                            }
                                                        %>
                                                    </div>
                                                    <%
                                                }
                                                %>
                                                <input class="btn btn-success btn-xs" type="submit" value="Confirmar">
                                            </form> 
                                        </div>
                                    <%
                                }
                                else{
                                    int noSePuede=0;
                                    String tabla = request.getParameter("tabla").toUpperCase();
                                    System.out.println("tabla "+tabla);
                                    int nColumnas = resultado.getMetaData().getColumnCount();
                                    ResultSet rtaPK = Conexion.getPrimaryKey(tabla);
                                    List<String> listaPK = new ArrayList<String>();
                                    while (rtaPK.next()) {
                                        String columnName = rtaPK.getString("COLUMN_NAME");
                                        listaPK.add(columnName);
                                    }
                                    for(int i=0;i<listaPK.size();i++){
                                        String sql = "";
                                        String tipoColumna = resultado.getMetaData().getColumnTypeName(resultado.findColumn(listaPK.get(i)));
                                        if(tipoColumna.equalsIgnoreCase("date")){
                                            sql = "Select count("+listaPK.get(i)+") as CUENTA from "+request.getParameter("tabla")+" where "+listaPK.get(i)+" = to_date('"+request.getParameter(listaPK.get(i))+"','yyyy/mm/dd')";
                                        }else{
                                            sql = "Select count("+listaPK.get(i)+") as CUENTA from "+request.getParameter("tabla")+" where "+listaPK.get(i)+"="+request.getParameter(listaPK.get(i));
                                        }
                                        System.out.println(sql);
                                        ResultSet rta = Conexion.consultaDB(sql);
                                        //rta.getInt(1);
                                        //while(rta.next()){
                                        //rta.beforeFirst();
                                        if(rta.next()){
                                            System.out.println("Repeticiones del valor: "+rta.getString(1));
                                        }
                                        //}
                                        if(!rta.getString(1).equalsIgnoreCase("0")){
                                            noSePuede++;
                                        }                                        
                                    }
                                    if(listaPK.size()==noSePuede){
                                        //NO SE PUEDE INSERTAR.
                                        System.out.println("NO SE PUEDE AGREGAR VALORES REPETIDOS EN LAS PK");
                                    }
                                    else{
                                        //SE PUEDE INSERTAR.
                                        String cadenaValores="";
                                        for(int i=1;i<=nColumnas;i++){                                            
                                            String nombreColumna = resultado.getMetaData().getColumnLabel(i);
                                            String tipoColumna = resultado.getMetaData().getColumnTypeName(i);
                                            if(i<nColumnas){
                                                if(tipoColumna.equalsIgnoreCase("date")){
                                                    if(request.getParameter(nombreColumna).equalsIgnoreCase(null)){
                                                        cadenaValores+="null,";
                                                    }else{
                                                        cadenaValores+="to_date('"+request.getParameter(nombreColumna)+"','yyyy/mm/dd'),";
                                                    }
                                                }
                                                else{
                                                    cadenaValores+="'"+request.getParameter(nombreColumna)+"',";
                                                }                                                
                                            }else{
                                                if(tipoColumna.equalsIgnoreCase("date")){
                                                    if(request.getParameter(nombreColumna).equalsIgnoreCase(null)){
                                                        cadenaValores+="null";
                                                    }else{
                                                        cadenaValores+="to_date('"+request.getParameter(nombreColumna)+"','yyyy/mm/dd')";
                                                    }
                                                }
                                                else{
                                                    cadenaValores+="'"+request.getParameter(nombreColumna)+"'";
                                                } 
                                            }
                                        }
                                        String insert = "INSERT INTO "+tabla+" VALUES ("+cadenaValores+")";
                                        System.out.println("Insertar->"+insert);
                                        Conexion.actualizar(insert);
                                        //Conexion.commit();
                                        response.sendRedirect("LoginDB.jsp"); 
                                    }
                                    
                                }                            
                            }
                        }
                    %>                    
                </div>
                <jsp:include page="Templates/right.jsp"></jsp:include>
            </div>
        </div>            
        <jsp:include page="Templates/footer.jsp"></jsp:include>        
    </body>        
    
</html>
