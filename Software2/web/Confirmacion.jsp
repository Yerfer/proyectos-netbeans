<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Principal.Conexion"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href="Templates/signin.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirmacion</title>
    </head>
    <body>
        <jsp:include page="Templates/header.jsp"></jsp:include>
        <div class="form-group block-center container-fluid text-center" style="margin: 0 auto 0; max-width: 50%;" >
            <H2 style="text-align: center;">Proceso culmidado de Forma Exitosa!!!</H2>
            <% 
                //EJEMPLO de .java
                //Conexion.getConexion();
            %>
            <h3>Usuario <%=  request.getParameter("nombres")+" "+request.getParameter("apellidos")   %></h3>
            <FORM METHOD="POST" ACTION="Formulario.jsp" >
                <div class="col-md-12" style="margin-top: 10px;">
                    <button class="btn btn-info" type="submit">Regresar</button>
                </div>
            </FORM>
            
        </div>
        
        <jsp:include page="Templates/footer.jsp"></jsp:include>
    </body>
</html>
