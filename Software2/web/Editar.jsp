<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href="Templates/signin.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar</title>
    </head>
    
    
    <body BGCOLOR="#ffffcc">
        <jsp:include page="Templates/header.jsp"></jsp:include>              
        <div class="container-fluid text-center">
            <div class="row content">
                <jsp:include page="Templates/left.jsp"></jsp:include>  
                <% 
                    if (Conexion.existeConexionDB()==false) { 
                        response.sendRedirect("LoginDB.jsp");   
                    }
                %> 
                <div class="col-sm-10 text-left">                       
                    <%
                        if (request.getParameter("tabla")==null){
                            %>

                            <%
                        }
                        else{
                            ResultSet resultado=null;
                            if(request.getParameter("tabla").equalsIgnoreCase("PRESTAMO")){
                                response.sendRedirect("EditarPrestamo.jsp?id="+request.getParameter("id")+"&tabla=prestamo");  
                            }else if(request.getParameter("tabla").equalsIgnoreCase("socio")){
                                resultado = Conexion.consultaDB("Select * from socio where socio="+request.getParameter("id"));
                            }else if(request.getParameter("tabla").equalsIgnoreCase("libro")){
                                resultado = Conexion.consultaDB("Select * from libro where libro="+request.getParameter("id"));
                            }else{
                                response.sendRedirect("LoginDB.jsp"); 
                            }
                            
                            if(resultado==null){
                                response.sendRedirect("Index.jsp");
                            }
                            else{
                                if(request.getParameter(resultado.getMetaData().getColumnLabel(2))==null){
                                    %> 
                                        <div class="" style="margin: 0 auto 0; max-width: 10%;">
                                            <h2><%= request.getParameter("tabla").toUpperCase() %></h2>
                                        </div>
                                        <div class="col-sm-10  block-center">
                                            <form method="post" action="Editar.jsp">
                                                <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                <input type="hidden" name="id" value="<%= request.getParameter("id") %>" />
                                                <% 

                                                int nullable = 0;
                                                int nColumnas = resultado.getMetaData().getColumnCount();
                                                if(resultado.next()){
                                                    System.out.println("Repeticiones del valor: "+resultado.getString(1));
                                                }
                                                for (int i = 2; i <= nColumnas; i++) {
                                                    System.out.println("DENTRO DEL FOR");
                                                    String tipoColumna = resultado.getMetaData().getColumnTypeName(i);
                                                    String nombreColumna = resultado.getMetaData().getColumnLabel(i);
                                                    nullable = resultado.getMetaData().isNullable(i);
                                                    %>
                                                    <div class="input-group">
                                                        <span for="<%= nombreColumna %>" class="input-group-addon"><%= nombreColumna %></span>
                                                        <%
                                                            if(nullable==ResultSetMetaData.columnNoNulls){
                                                                System.out.println("DENTRO DEL UN IF");
                                                                %>
                                                                <input id="<%= nombreColumna %>" required type="<%= tipoColumna.toLowerCase() %>" class="form-control" name="<%= nombreColumna %>" placeholder="<%= resultado.getString(i) %>">
                                                                <%
                                                            }else{
                                                                %>
                                                                    <input id="<%= nombreColumna %>" type="<%= tipoColumna.toLowerCase() %>" class="form-control" name="<%= nombreColumna %>" placeholder="<%= resultado.getString(i) %>">
                                                                <%
                                                            }
                                                        %>
                                                    </div>
                                                    <%
                                                }
                                                %>
                                                <input class="btn btn-success btn-xs" type="submit" value="Actualizar">
                                            </form> 
                                        </div>
                                    <%
                                }
                                else{
                                    System.out.println("DENTRO DEL ELSE PARA ACTUALIZAR");
                                    if(request.getParameter("tabla").equalsIgnoreCase("socio")){
                                        System.out.println("DENTRO DEL IF TABLA PARA ACTUALIZAR");
                                        int conta=0;
                                        ResultSet resultado1 = Conexion.consultaDB("Select count(socio) from socio where socio="+request.getParameter("socio"));
                                        if(resultado1.next()){
                                            conta= resultado1.getInt(1);
                                        }
                                        if(conta>0){
                                            System.out.println("NO SE PUEDE INSERTAR");
                                            response.sendRedirect("Index.jsp");
                                        }
                                        else{
                                            //Update nombre_tabla Set nombre_campo1 = valor_campo1, nombre_campo2 = valor_campo2,... Where condiciones_de_selección
                                            String sql = "Update SOCIO set nombre='"+request.getParameter("NOMBRE")+"', telefono='"+request.getParameter("TELEFONO")+"', ingreso=to_date('"+request.getParameter("INGRESO")+"','yyyy/mm/dd') "
                                                    + "where socio="+request.getParameter("id");
                                            Conexion.actualizar(sql);
                                            System.out.println("SQL: "+sql);
                                        }
                                        System.out.println("POR AHÏIIII");
                                    }else if(request.getParameter("tabla").equalsIgnoreCase("libro")){
                                        int conta=0;
                                        ResultSet resultado2 = Conexion.consultaDB("Select count(libro) from libro where libro="+request.getParameter("libro"));
                                         if(resultado2.next()){
                                            conta= resultado2.getInt(1);
                                        }
                                        if(conta>0){
                                            System.out.println("NO SE PUEDE INSERTAR");
                                            response.sendRedirect("Index.jsp");
                                        }
                                        else{
                                            String sql = "Update LIBRO set titulo='"+request.getParameter("TITULO")+"', autor='"+request.getParameter("AUTOR")+"', numpag="+request.getParameter("NUMPAG")
                                                    + "where libro="+request.getParameter("id");
                                            Conexion.actualizar(sql);
                                            System.out.println("SQL: "+sql);
                                        }
                                        System.out.println("ACHUUUUU");
                                    }
                                        //String insert = "INSERT INTO "+tabla+" VALUES ("+cadenaValores+")";
                                        //System.out.println("Insertar->"+insert);
                                        //Conexion.actualizar(insert);
                                        //Conexion.commit();
                                        response.sendRedirect("Index.jsp"); 
                                }                            
                            }
                        }
                    %>                    
                </div>
                <jsp:include page="Templates/right.jsp"></jsp:include>
            </div>
        </div>            
        <jsp:include page="Templates/footer.jsp"></jsp:include>        
    </body>        
    
</html>

