<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%>
<%@page import="Principal.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- ESTA ES UNA VERSIÓN ALTERNATIVA EN DONDE SE OMITE LA VALIDACIÓN DE PK, PARA INTENTAR INSERTAR DE UNA
    PARA QUE NO FALLE SE DEBE TENER UN VALIDADOR O CONTROLADOR EXTERNO A ESTA CLASE, 
    ACÁ DEBEN LLEGAR LOS DATOS CORRECTOS DIRECTOS PARA INSERTAR
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar</title>
    </head>
    
    
    <body BGCOLOR="#ffffcc">
        <%  
            ResultSet resultado = Conexion.consultaDB("Select * from "+request.getParameter("tabla"));
            if(resultado==null){
                response.sendRedirect("Home.jsp");
            }
            int noSePuede=0;
            String tabla = request.getParameter("tabla").toUpperCase();
            System.out.println("tabla "+tabla);
            int nColumnas = resultado.getMetaData().getColumnCount();
            ResultSet rtaPK = Conexion.getPrimaryKey(tabla);
            List<String> listaPK = new ArrayList<String>();
            while (rtaPK.next()) {
                String columnName = rtaPK.getString("COLUMN_NAME");
                listaPK.add(columnName);
            }
            for(int i=0;i<listaPK.size();i++){
                String sql = "";
                String tipoColumna = resultado.getMetaData().getColumnTypeName(resultado.findColumn(listaPK.get(i)));
                if(tipoColumna.equalsIgnoreCase("date")){
                    sql = "Select count("+listaPK.get(i)+") as CUENTA from "+request.getParameter("tabla")+" where "+listaPK.get(i)+" = to_date('"+request.getParameter(listaPK.get(i))+"','yyyy/mm/dd')";
                }else{
                    sql = "Select count("+listaPK.get(i)+") as CUENTA from "+request.getParameter("tabla")+" where "+listaPK.get(i)+"="+request.getParameter(listaPK.get(i));
                }
                System.out.println(sql);
                ResultSet rta = Conexion.consultaDB(sql);
                //rta.getInt(1);
                //while(rta.next()){
                //rta.beforeFirst();
                if(rta.next()){
                    System.out.println("Repeticiones del valor: "+rta.getString(1));
                }
                //}
                if(!rta.getString(1).equalsIgnoreCase("0")){
                    noSePuede++;
                }                                        
            }
            noSePuede=0;
            if(listaPK.size()==noSePuede){
                //NO SE PUEDE INSERTAR.
                System.out.println("NO SE PUEDE AGREGAR VALORES REPETIDOS EN LAS PK");                
                response.sendRedirect("Home.jsp?repite=false");
            }
            else{
                //SE PUEDE INSERTAR.
                System.out.println("Se puede Insertar");
                String cadenaValores="";
                for(int i=1;i<=nColumnas;i++){  
                    String nombreColumna = resultado.getMetaData().getColumnLabel(i);
                    String tipoColumna = resultado.getMetaData().getColumnTypeName(i);
                    System.out.println("Columna: "+nombreColumna+" tipo: "+tipoColumna);
                    if(i<nColumnas){
                        if(tipoColumna.equalsIgnoreCase("date")){
                            if(request.getParameter(nombreColumna).equalsIgnoreCase(null)){
                                cadenaValores+="null,";
                            }else{
                                cadenaValores+="to_date('"+request.getParameter(nombreColumna)+"','yyyy/mm/dd'),";
                            }
                        }
                        else{
                            cadenaValores+="'"+request.getParameter(nombreColumna)+"',";
                        }                                                
                    }else{
                        if(tipoColumna.equalsIgnoreCase("date")){
                            if(request.getParameter(nombreColumna).equalsIgnoreCase(null)){
                                cadenaValores+="null";
                            }else{
                                cadenaValores+="to_date('"+request.getParameter(nombreColumna)+"','yyyy/mm/dd')";
                            }
                        }
                        else{
                            cadenaValores+="'"+request.getParameter(nombreColumna)+"'";
                        } 
                    }
                }
                String insert = "INSERT INTO "+tabla+" VALUES ("+cadenaValores+")";
                System.out.println("Insertar->"+insert);
                Conexion.actualizar(insert);
                //Conexion.commit();
                //request.setAttribute("confirma", true);
                //request.getRequestDispatcher("FormularioEmpleado.jsp").forward(request, response);
                response.sendRedirect("Home.jsp?confirma=true"); 
            }
        %>
    </body>        
    
</html>

