<%@page import="Principal.Conexion"%>
<%@page import="java.sql.ResultSet"%>
<div class="well form-group block-center container-fluid text-justify img-responsive" id="divEntradaAlmacen" style="margin: 0 auto 0; max-width: 80%; max-height: 80%; " >
    <!--<FORM METHOD="POST" ACTION="Agregar2.jsp" >
        <input type="hidden" name="tabla" value="ELEMENTOS" />
        <input type="hidden" name="CODIGO" value="-1" />-->
        <%
            String idEmpleado = "";
            idEmpleado = request.getParameter("idEmpleado");
            String entradaID = "select ID_ENTRADA from ENTRADA_ALMACEN where RESPONSABLE='"+idEmpleado+"' and ROWNUM<=1 ORDER BY 1 desc";
            ResultSet resultadoEntradas = Conexion.consultaDB(entradaID);
            while(resultadoEntradas.next()){    
                %>
                    <input type="hidden" id="idEntrada" value="<%= resultadoEntradas.getString(1) %>"/>
                <%
            }
            %>
        <input type="hidden" id="idEmpleado" value="<%= request.getParameter("idEmpleado") %>"/>
        <div class="col-md-12"> 
            <div class="col-md-12" style="text-align: center;">
                <h2>DETALLE ENTRADA</h2>
            </div>
            <div class="form-group col-md-12" style="">
                <div class="col-md-2" style="margin-left: 0%;">                    
                    <label for="elementoID">Elemento</label>   
                </div>
                <div class="col-md-10" style="padding-left: 0px; padding-right: 0px;">
                    <select name="elementoID" id="elementoID" class="form-control" required>
                            <option value="">-- Elemento --</option>
                            <%
                                String sql = "select l.CODIGO, l.ELEMENTO from ELEMENTOS l";
                                //sql = "select t.ID_ENTRADA from ENTRADA_ALMACEN t where t.RESPONSABLE="+1212+" and ROWNUM<=1 ORDER BY 1 desc";
                                ResultSet resultado = Conexion.consultaDB(sql);
                                if(resultado==null){
                                    %>
                                        <option value="">Sin Datos</option>
                                    <%
                                }
                                while(resultado.next()){                                            
                                    %>
                                        <option value="<%= resultado.getString(1)  %>"><%= resultado.getString(2)  %></option>
                                    <%
                                }
                            %>
                    </select>                
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <label for="cantidadInput">Cantidad </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="number" name="cantidadInput" id="cantidadInput" placeholder="CANTIDAD"/>
                </div>
                <div class="col-md-3">
                    <label for="vUnitarioInput">V_Unitario </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="number" name="vUnitarioInput" id="vUnitarioInput" placeholder="V_UNITARIO"/>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <label for="subtotalInput">Subtotal </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="number" name="subtotalInput" id="subtotalInput" placeholder="SUBTOTAL"/>
                </div>
                <div class="col-md-3">
                    <label for="ivaDetalleInput">IVA </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="number" name="ivaDetalleInput" id="ivaDetalleInput" placeholder="IVA"/>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-3">
                    <label for="entregadosInput">Entregados </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="number" name="entregadosInput" id="entregadosInput" placeholder="ENTREGADOS"/>
                </div>
                <div class="col-md-3">
                    <label for="pedidoInput">Pedido </label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <select name="pedidoInput" id="pedidoInput" class="form-control" required>
                            <option value="">-- Elemento --</option>
                            <option value="0">-- 0 --</option>
                            <%
                                String pedidos = "select p.ID_PEDIDO from PEDIDOS p";
                                ResultSet resultadoPedidos = Conexion.consultaDB(pedidos);
                                if(resultadoPedidos==null){
                                    %>
                                        <option value="">Sin Datos</option>
                                    <%
                                }
                                while(resultadoPedidos.next()){                                            
                                    %>
                                        <option value="<%= resultadoPedidos.getString(1)  %>"><%= resultadoPedidos.getString(1)  %></option>
                                    <%
                                }
                            %>
                    </select>                
                </div>
            </div>              
        </div>
                
        <div class="clearfix"> </div>
        <hr style="border-top: 1px solid gray;">
        <%// ac� van los botones%>
        <div style="text-align: center;">
            <!--<button style="margin: 10px" class="btn btn-info" id="verdetalleEntrada">Ver Detalle</button>-->
            <button style="margin: 10px" class="btn btn-danger" id="cancelarDetalle">Cancelar</button>
            <button style="margin: 10px" class="btn btn-success" id="agregarDetalle">Agregar</button>
            <button style="margin: 10px" class="btn btn-primary" id="finalizarDetalle">Finalizar</button>
        </div>
    <!--</FORM>-->
    
</div>
        
<!--<div id="cargadetalle"> </div>-->
        
<script>
    
    $(document).ready(function(){    
        var conta = 1;
        $("#agregarDetalle").click(function(){
            if ($("#elementoID").val()===""||$("#cantidadInput").val()===""
                    ||$("#vUnitarioInput").val()===""||$("#subtotalInput").val()===""||
                    $("#ivaDetalleInput").val()===""||$("#entregadosInput").val()===""||
                    $("#pedidoInput").val()==="") { 
                alert("LLENE TODOS LOS CAMPOS!!!");
            }else{
                $.post("Agregar2_Alternativo.jsp",{tabla:"DETALLE_ENTRADA",ID_ENTRADA:$("#idEntrada").val(),NUM_DETALLE:conta++,  //HACIENDO..
                    ELEMENTO:$("#elementoID").val(),CANTIDAD:$("#cantidadInput").val(),
                    V_UNITARIO:$("#vUnitarioInput").val(),SUBTOTAL:$("#subtotalInput").val(),IVA:$("#ivaDetalleInput").val(),
                    ENTREGADOS:$("#entregadosInput").val(),ID_PEDIDO:$("#pedidoInput").val()
                });    
                alert("Inserci�n Exitosa!");
                
                //LIMPIAR LOS INPUTS
                $("#elementoID").val("");
                $("#cantidadInput").val("");
                $("#vUnitarioInput").val("");
                $("#subtotalInput").val("");
                $("#ivaDetalleInput").val("");
                $("#entregadosInput").val("");
                $("#pedidoInput").val("");             
            }
            
            $.post("VerDetalle.jsp",{},function(htmlexterno2){
                $("#cargadetalle").html(htmlexterno2);
            });
        });
	$("#finalizarDetalle").click(function(){
            $("#cargaDetalleEntrada").hide(3000);
            $("#cargaEntradaAlmacen").hide(3000);
            $("#divResponsable").show(3000);
            $("#empleadoID").val("");
        });
        $("#cancelarDetalle").click(function(){
            $("#cargaDetalleEntrada").hide(3000);
            $("#cargaEntradaAlmacen").show(3000);
        });
    });
</script>