<div class="well form-group block-center container-fluid text-justify img-responsive" style="margin: 0 auto 0; max-width: 80%; max-height: 80%;">
    <div>
        <p>
            1. Pedidos realizados por cada empleado, mostrar: identificaci�n, nombres del empleado, 
            c�digo de pedido, el elemento solicitado y la fecha en que realizo el pedido, ordenar los 
            datos por el nombre del empleado en orden alfab�tico y por la fecha de pedido en orden 
            descendente.
            <br/>
            2. Pedidos realizados por cada empleado que fueron aprobados, mostrar: identificaci�n, 
            nombres del empleado, id_pedido y el elemento aprobado, ordenar los datos por el nombre 
            del empleado en orden alfabetico y por el id_pedido en orden descendente. 
            <br/>
            3. Pedidos realizados por cada empleado que no fueron aprobados, mostrar: identificaci�n, 
            nombres del empleado y los elementos.
            <br/>
            4. Listado de pedidos realizados (mostrar toda la informaci�n de la tabla pedidos incluyendo el 
            nombre del elemento y el nombre completo del empleado), ordenar los datos de la siguiente 
            forma: primero los elementos aprobados y estos a su vez por la fecha de pedido de forma descendente.
            <br/>
            5. Total de pedidos por fecha, es decir, cantidad de elementos solicitados en un d�a.
            <br/>
            6. Listado de personas que solicitaron el elemento "CASCO MAC", de cada empleado se 
            necesita conocer: la identificaci�n, nombre completo y cargo actual.
            <br/>
            7. Listado de personas que no les fue aprobado alg�n pedido, de cada empleado se necesita 
            conocer: la identificaci�n, nombre completo, cargo actual y el elemento solicitado, y la fecha en que realizo el pedido. 
            <br/>
            8. Listado elementos solicitados y la cantidad de veces que ha sido solicitado, del elemento se 
            desea conocer el c�digo y el nombre.
            <br/>
            9. Listar el elemento m�s solicitado y la cantidad de veces que ha sido solicitado, del elemento 
            se desea conocer el c�digo y el nombre.
            <br/>
            10. Listado de entradas realizadas.
            <br/>
            11. Total de entradas por fecha, es decir cu�ntas entradas se realizaron por dia.
            <br/>
            12. Cantidad de elementos que ingresaron en un d�a, mostrar c�digo de elemento, nombre del elemento y el total.
            <br/>
            13. Listado de elementos que entraron al almac�n, del listado se desea conocer el 
            id_entrada, el detalle, el nombre del elemento, la cantidad de elementos, el valor unitario, el 
            subtotal, calcular el valor del IVA que es del 16% sobre el subtotal y calcular el valor total, 
            todo se debe hacer en la consulta sql.
            <br/>
            14. Listar el valor promedio de cada elemento, no tener en cuenta el IVA.
            <br/>
            15. Listado de funcionarios que solicitaron (que realizaron pedidos) elementos que no se les 
            hab�a asignado, del listado se desea conocer el nombre del funcionario, la identificaci�n y elemento que solicito.
            <br/>
            16. Listar los elementos entregados entre el 01/02/2009 y el 15/10/2009, y la cantidad entregada de cada elemento.
            <br/>
            17. Listar los elementos entregados entre el 01/02/2009 y el 15/10/2009, y la cantidad 
            entregada de cada elemento, pero solamente mostrar aquellos elementos de los cuales se halla entregado m�s de uno.
            <br/>
            18. Listar los cargos y el total de elementos solicitados por los empleados que tienen 
            asignado un cargo, es decir, se debe mostrar: el cargo, el elemento y el total solicitado. 
            <br/>
        </p>
    </div>
    <%
        String consultas[] = new String[18];
            //---1---
        consultas[0]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, "
                            + "p.ID_PEDIDO, l.ELEMENTO, p.FECHA "
                + "from empleados e, pedidos p, elementos l "
                + "where e.IDENTIFICACION=p.EMPLEADO and p.ELEMENTO=l.CODIGO "
                + "order by e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 ASC, p.FECHA DESC";
            //---2---
        consultas[1]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, p.ID_PEDIDO, l.ELEMENTO, p.APROBADO "
                + "from empleados e, pedidos p, elementos l "
                + "where e.IDENTIFICACION=p.EMPLEADO and p.APROBADO='S' and p.ELEMENTO=l.CODIGO "
                + "order by e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 ASC, p.ID_PEDIDO DESC";

            //---3---
        consultas[2]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, p.ID_PEDIDO, l.ELEMENTO, p.APROBADO "
        + "from empleados e, pedidos p, elementos l "
        + "where e.IDENTIFICACION=p.EMPLEADO and p.APROBADO='N' and p.ELEMENTO=l.CODIGO "
        + "order by e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 ASC, p.ID_PEDIDO DESC";

            //---4---
        consultas[3]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, p.ID_PEDIDO, l.ELEMENTO, p.FECHA, p.CANTIDAD, p.APROBADO, p.LLEGO "
                + "from empleados e, pedidos p, elementos l "
                + "where e.IDENTIFICACION=p.EMPLEADO and p.ELEMENTO=l.CODIGO "
                + "order by p.APROBADO DESC, p.FECHA DESC";

            //---5---
        consultas[4]="select l.ELEMENTO, to_char(p.FECHA,'dd/mm/yyyy') FECHA, count(l.ELEMENTO) TOTAL_PEDIDOS "
                + "from pedidos p, elementos l "
                + "where p.ELEMENTO=l.CODIGO "
                + "group by l.ELEMENTO, to_char(p.FECHA,'dd/mm/yyyy') "
                + "order by to_char(p.FECHA,'dd/mm/yyyy') desc, l.ELEMENTO asc";

            //---6---
        consultas[5]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, c.CARGO, p.ID_PEDIDO, l.ELEMENTO, p.FECHA "
                + "from empleados e, pedidos p, elementos l, cargos c, historial_laboral h "
                + "where e.IDENTIFICACION=p.EMPLEADO and e.IDENTIFICACION=h.EMPLEADO and h.CARGO=c.COD_CARGO and p.ELEMENTO=l.CODIGO and l.ELEMENTO='CASCO MAC' and p.EMPLEADO=h.EMPLEADO and h.ACTUAL='S'";

            //---7---
        /*consultas[6]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 "NOMBRE EMPLEADO", c.CARGO, p.ID_PEDIDO, l.ELEMENTO, to_char(p.FECHA,'dd/mm/yyyy') FECHA, p.APROBADO
            from empleados e, pedidos p, elementos l, cargos c, historial_laboral h
            where e.IDENTIFICACION=p.EMPLEADO and e.IDENTIFICACION=h.EMPLEADO and h.CARGO=c.COD_CARGO and p.ELEMENTO=l.CODIGO and p.APROBADO='N' and p.EMPLEADO=h.EMPLEADO and h.ACTUAL='S'
            group by e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2, c.CARGO, p.ID_PEDIDO, l.ELEMENTO, to_char(p.FECHA,'dd/mm/yyyy'), p.APROBADO
            order by 2 ASC, 6 DESC;*/

            //---Otra Forma...
        consultas[6]="select distinct e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, c.CARGO, p.ID_PEDIDO, l.ELEMENTO, to_char(p.FECHA,'dd/mm/yyyy') FECHA, p.APROBADO "
                + "from empleados e, pedidos p, elementos l, cargos c, historial_laboral h "
                + "where e.IDENTIFICACION=p.EMPLEADO and e.IDENTIFICACION=h.EMPLEADO and h.CARGO=c.COD_CARGO and p.ELEMENTO=l.CODIGO and p.APROBADO='N' and p.EMPLEADO=h.EMPLEADO and h.ACTUAL='S' "
                + "order by 2 ASC, 6 DESC";

            //---8---
        consultas[7]="select l.CODIGO, l.ELEMENTO, count(l.ELEMENTO) VECES_SOLICITADO "
                + "from elementos l, pedidos p "
                + "where l.CODIGO=p.ELEMENTO "
                + "group by l.CODIGO, l.ELEMENTO "
                + "order by 1 ASC";

            //---9---
        consultas[8]="select * "
                + "from ( "
                    + "select l.CODIGO, l.ELEMENTO, count(l.ELEMENTO) VECES_SOLICITADO "
                    + "from elementos l, pedidos p "
                    + "where l.CODIGO=p.ELEMENTO "
                    + "group by l.CODIGO, l.ELEMENTO "
                    + "order by 3 DESC"
                + ") "
                + "where rownum <=1";

            //---10---
        consultas[9]="select * "
                + "from entrada_almacen r "
                + "order by r.ID_ENTRADA";

            //---11---
        consultas[10]="select to_char(r.FECHA,'dd/mm/yyyy') FECHA, count(to_char(r.FECHA,'dd/mm/yyyy')) TOTAL_DE_ENTRADAS "
                + "from entrada_almacen r "
                + "group by to_char(r.FECHA,'dd/mm/yyyy') "
                + "order by 1 DESC";

            //---12---
        consultas[11]="select l.CODIGO, l.ELEMENTO, sum(d.CANTIDAD) CANTIDAD_DE_ENTRADAS, to_char(r.FECHA,'dd/mm/yyyy/') FECHA "
                + "from entrada_almacen r, detalle_entrada d, elementos l "
                + "where r.ID_ENTRADA=d.ID_ENTRADA and d.ELEMENTO=l.CODIGO "
                + "group by l.CODIGO, l.ELEMENTO, to_char(r.FECHA,'dd/mm/yyyy/') "
                + "order by 1 ASC, 4 DESC";

            //---13---
        consultas[12]="select r.ID_ENTRADA, d.NUM_DETALLE, l.ELEMENTO, d.CANTIDAD, d.V_UNITARIO, d.SUBTOTAL, d.SUBTOTAL*0.16 IVA, d.SUBTOTAL*1.16 VALOR_TOTAL "
                + "from entrada_almacen r, detalle_entrada d, elementos l "
                + "where r.ID_ENTRADA=d.ID_ENTRADA and d.ELEMENTO=l.CODIGO "
                + "order by 1 DESC";

            //---14---
        consultas[13]="select l.CODIGO, l.ELEMENTO, avg(d.V_UNITARIO) VALOR_PROMEDIO "
                + "from entrada_almacen r, detalle_entrada d, elementos l "
                + "where r.ID_ENTRADA=d.ID_ENTRADA and d.ELEMENTO=l.CODIGO "
                + "group by l.CODIGO,l.ELEMENTO order by 1 ASC";

            //---15---
        consultas[14]="select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, l.ELEMENTO "
                + "from empleados e, pedidos p, elementos l "
                + "where e.IDENTIFICACION=p.EMPLEADO and  p.ELEMENTO=l.CODIGO "
                + "minus "
                + "select e.IDENTIFICACION, e.NOMBRE_1||' '||e.NOMBRE_2||' '||e.APELLIDO_1||' '||e.APELLIDO_2 NOMBRE_EMPLEADO, l.ELEMENTO "
                + "from empleados e, elementos_asignados s, elementos l "
                + "where e.IDENTIFICACION=s.EMPLEADO and s.ELEMENTO=l.CODIGO and s.ACTUAL='S'";

            //---16---
        consultas[15]="select l.CODIGO, l.ELEMENTO, count(l.ELEMENTO) CANTIDAD_ENTREGADA "
                    + "from elementos l, entrega_elementos t "
                    + "where l.CODIGO=t.ELEMENTO and t.FECHA between to_date('01/02/2009','dd/mm/yyyy') and to_date('15/10/2009','dd/mm/yyyy') "
                    + "group by l.CODIGO, l.ELEMENTO "
                    + "order by 1 ASC";

            //---17---
            /*consultas[16]="select *
            from (
                select l.CODIGO, l.ELEMENTO, count(l.ELEMENTO) CANTIDAD_ENTREGADA
                from elementos l, entrega_elementos t
                where l.CODIGO=t.ELEMENTO and t.FECHA between to_date('01/02/2009','dd/mm/yyyy') and to_date('15/10/2009','dd/mm/yyyy')
                group by l.CODIGO, l.ELEMENTO
            )
            where CANTIDAD_ENTREGADA>1;*/

            //---Otra forma...
        consultas[16]="select l.CODIGO, l.ELEMENTO, count(l.ELEMENTO) CANTIDAD_ENTREGADA "
                + "from elementos l, entrega_elementos t "
                + "where l.CODIGO=t.ELEMENTO and t.FECHA between to_date('01/02/2009','dd/mm/yyyy') and to_date('15/10/2009','dd/mm/yyyy') "
                + "group by l.CODIGO, l.ELEMENTO "
                + "having count(l.ELEMENTO)>1 "
                + "order by 1 ASC";

            //---18---
        consultas[17]="select c.CARGO, l.ELEMENTO, sum(p.CANTIDAD) TOTAL_DE_ELEMENTOS_SOLICITADOS "
                + "from elementos l, cargos c, historial_laboral h, pedidos p "
                + "where l.CODIGO=p.ELEMENTO and p.EMPLEADO=h.EMPLEADO and c.COD_CARGO=h.CARGO and h.ACTUAL='S' "
                + "group by c.CARGO, l.ELEMENTO "
                + "order by 1 ASC, 2 ASC";

    %>
    <div>
        <FORM METHOD="POST" ACTION="Index2.jsp">
            <!--<input type="hidden" name="indice" value="i" />-->
            <div class="col-md-12"> 
                <div class="form-group col-md-12" style="padding-left: 34%; padding-right: 0px;">
                    <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                        <label  for="consulta">CONSULTA</label>   
                    </div>
                    <!--<div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                        <input class="form-control" type="number" name="consulta" id="consulta" min="1" max="18"  placeholder="1-18" required>
                    </div>-->
                    <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                        <select name="consulta" id="consulta" class="form-control" required>
                            <option value="">Selecciona</option>
                            <%
                                for (int i = 0; i < 18; i++) {
                                    %>
                                        <option value="<%= consultas[i] %>"><%= i+1 %></option>
                                    <%
                                }
                            %>
                        </select>
                    </div>
                </div>  
                <div style="text-align: center;">
                    <button style="margin: 10px" class="btn btn-danger" type="reset">Cancelar</button>
                    <button style="margin: 10px" class="btn btn-success" id="botonConsultar" type="submit" >Consultar</button>
                </div>
            </div>
        </form>
    </div>
</div>
            
<div id="resultadoConsulta"></div>

<script>
    $(document).ready(function(){
	$("#botonConsultar").click(function(){           
            $.post("Index2.jsp", {empleadoID:$("#empleadoID").val()}, function(htmlexterno){ //FALTA HACER ESTA VISTA DESTINO Y OTRA M�S.
                $("#resultadoConsulta").html(htmlexterno);
            });
        });
    });
</script>