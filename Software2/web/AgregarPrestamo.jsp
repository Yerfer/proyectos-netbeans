<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href="Templates/signin.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar</title>
    </head>
    
    
    <body BGCOLOR="#ffffcc">
        <jsp:include page="Templates/header.jsp"></jsp:include>              
        <div class="container-fluid text-center">
            <div class="row content">
                <jsp:include page="Templates/left.jsp"></jsp:include>  
                <% 
                    if (Conexion.existeConexionDB()==false) { 
                        response.sendRedirect("LoginDB.jsp");   
                    }
                %> 
                <div class="col-sm-10 text-left">                       
                    <%
                        if (request.getParameter("tabla")==null){
                        //if(false){
                            response.sendRedirect("LoginDB.jsp");
                        }
                        else{
                            System.out.println("asasasasasasasasasas");
                            ResultSet resultado = Conexion.consultaDB("Select * from PRESTAMO");
                            ResultSet libro = Conexion.consultaDB("Select libro,titulo from LIBRO");
                            ResultSet socio = Conexion.consultaDB("Select socio,nombre from SOCIO");
                            if(resultado==null){
                                response.sendRedirect("Index.jsp");
                            }else{
                                System.out.println("zxzxxzxzzxxzzxxzxz: "+resultado.getMetaData().getColumnLabel(1));
                                System.out.println("zxzxxzxzzxxzzxxzxz: "+request.getParameter(resultado.getMetaData().getColumnLabel(1)));
                                System.out.println("**************** "+request.getParameter("LIBRO"));
                                if(request.getParameter(resultado.getMetaData().getColumnLabel(1))==null){
                                    System.out.println("nnbbbbbbbbbb");
                                    %> 
                                        <div class="" style="margin: 0 auto 0; max-width: 10%;">
                                            <h2>PRÉSTAMO</h2>
                                        </div>
                                        <div class="col-sm-10  block-center">
                                            <form method="get" action="AgregarPrestamo.jsp">
                                                <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                <div class="input-group  block-center">
                                                    
                                                    <div>
                                                        <span for="LIBRO" class="input-group-addon">LIBRO</span>
                                                        <input list="LIBRO" name="LIBRO" required>                                                        
                                                        <datalist id="LIBRO">
                                                        <%
                                                            while(libro.next()){
                                                                %>
                                                                <option value="<%= libro.getString(1) %>"> <%= libro.getString(2) %> </option>                                                               
                                                                <%
                                                            }
                                                        %>
                                                        </datalist> 
                                                    </div>
                                                    <div>
                                                        <span for="SOCIO" class="input-group-addon">SOCIO</span>
                                                        <input list="SOCIO" name="SOCIO" required>                                                        
                                                        <datalist id="SOCIO">
                                                        <%
                                                            while(socio.next()){
                                                                %>
                                                                <option value="<%= socio.getString(1) %>"> <%= socio.getString(2) %> </option>                                                             
                                                                <%
                                                            }
                                                        %>
                                                        </datalist> 
                                                    </div>
                                                    <div>
                                                        <span for="FPRESTAMO" class="input-group-addon">FECHA DE PRÉSTAMO</span>
                                                        <input id="FPRESTAMO" required type="date" class="form-control" name="FPRESTAMO" placeholder="FPRESTAMO">
                                                    </div>
                                                    <div>
                                                        <span for="FDEVOLUCION" class="input-group-addon">FECHA DE DEVOLUCIÓN</span>
                                                        <input id="FDEVOLUCION" type="date" class="form-control" name="FDEVOLUCION" placeholder="FDEVOLUCION">
                                                    </div>
                                                <input class="btn btn-success btn-xs" type="submit" value="Confirmar">
                                            </form> 
                                        </div>
                                    <%
                                }
                                else{ 
                                    String libroRTA = request.getParameter("LIBRO");
                                    String socioRTA = request.getParameter("SOCIO");
                                    String fPrestamoRTA = request.getParameter("FPRESTAMO");
                                    String fDevolucionRTA = request.getParameter("FDEVOLUCION");
                                    /*String sql1 = "Select count(libro) as CUENTA from PRESTAMO where libro ="+libroRTA;
                                    String sql2 = "Select count(socio) as CUENTA from PRESTAMO where socio ="+socioRTA;
                                    String sql3 = "Select count(FPRESTAMO) as CUENTA from PRESTAMO where FPRESTAMO = to_date('"+fPrestamoRTA+"','yyyy/mm/dd')";
                                    System.out.println("00000000000000000");
                                    ResultSet rta1 = Conexion.consultaDB(sql1);
                                    ResultSet rta2 = Conexion.consultaDB(sql2);
                                    ResultSet rta3 = Conexion.consultaDB(sql3);
                                    System.out.println("111111111111111");
                                    if(rta1.next()){
                                        rta1.getString(1);
                                    }
                                    if(rta2.next()){
                                        rta2.getString(1);
                                    }
                                    if(rta3.next()){
                                        rta3.getString(1);
                                    }*/
                                    boolean existe=false;
                                    System.out.println("aaaaaaaaaaaaaaaaaaaaa");
                                    //if(!rta1.getString(1).equalsIgnoreCase("0") && !rta2.getString(1).equalsIgnoreCase("0") && !rta3.getString(1).equalsIgnoreCase("0")){
                                        //APARECEN LAS 3 PK pero aún sin saber si es la misma fila.
                                    while(resultado.next()){
                                        if(resultado.getString(1).equalsIgnoreCase(libroRTA) && resultado.getString(2).equalsIgnoreCase(socioRTA) &&  resultado.getString(3).equalsIgnoreCase("to_date('"+fPrestamoRTA+"','yyyy/mm/dd')")){
                                           existe=true; 
                                           System.out.println("ya exsite");
                                        }                                            
                                    }
                                    System.out.println("bbbbbbbbbbbbbbbbb");
                                    //} 
                                    if(existe){
                                        //NO SE PUEDE AGREGAR.
                                        System.out.println("NO SE PUEDE AGREGAR");
                                    }
                                    else{
                                        //SE PUEDE AGREGAR.
                                        String insert = "INSERT INTO PRESTAMO VALUES ('"+libroRTA+"','"+socioRTA+"', to_date('"+fPrestamoRTA+"','yyyy/mm/dd'),to_date('"+fDevolucionRTA+"','yyyy/mm/dd'))";
                                        System.out.println("Insertar->"+insert);
                                        Conexion.actualizar(insert);
                                        //Conexion.commit();
                                        response.sendRedirect("LoginDB.jsp"); 
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                }                            
                            }
                        }
                    %>                    
                </div>
                <jsp:include page="Templates/right.jsp"></jsp:include>
            </div>
        </div>            
        <jsp:include page="Templates/footer.jsp"></jsp:include>        
    </body>        
    
</html>