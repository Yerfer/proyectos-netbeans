<div style="text-align: center; text-shadow: 2px 2px 5px BLACK; color:white; margin: 24px; ">
    <h3>FORMULARIO PARA EL REGISTRO DE EMPLEADOS Y/O ACTUALIZACI�N DE INFORMACI�N   </h3>
</div>

<div class="well form-group block-center container-fluid text-justify img-responsive" style="margin: 0 auto 0; max-width: 80%; max-height: 80%; " >
    <FORM METHOD="POST" ACTION="Agregar2.jsp" >
        <input type="hidden" name="tabla" value="EMPLEADOS" />
        <div class="col-md-12"> 
            <div class="form-group col-md-12">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label  for="IDENTIFICACION">IDENTIFICACI�N</label>   
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input class="form-control" type="number" name="IDENTIFICACION" id="IDENTIFICACION"  placeholder="1111111" required>
                </div>
                <div class="col-md-3" style="padding-left: 10px; padding-right: 0px;">
                    <label for="TIPO">TIPO</label>
                </div>
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <select name="TIPO" class="form-control" required>
                        <option value="">Selecciona</option>
                        <option value="cc">C�dula de Ciudadan�a</option>
                        <option value="ce">C�dula de Extranger�a</option>
                        <option value="ti">Tarjeta de Identidad</option>                 
                    </select>
                </div>
            </div>                    
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="NOMBRE_1">PRIMER NOMBRE</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="NOMBRE_1" id="NOMBRE_1" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="Anacleto1" required>
                </div>
                <div class="col-md-3" style="padding-left: 10px; padding-right: 0px;">
                    <label for="NOMBRE_2">SEGUNDO NOMBRE</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="NOMBRE_2" id="NOMBRE_2" maxlength="50" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="Anacleto2">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="APELLIDO_1">PRIMER APELLIDO</label>
                </div>                        
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="APELLIDO_1" id="APELLIDO_1" size="50" maxlength="50" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="Eustaquio1" required>
                </div>
                <div class="col-md-3" style="padding-left: 10px; padding-right: 0px;">
                    <label for="APELLIDO_2">SEGUNDO APELLIDO</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="APELLIDO_2" id="APELLIDO_2" size=15 maxlength="50" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="Eustaquio2" required>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="SEXO">SEXO</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <select name="SEXO" class="form-control" required>
                        <option value="">Selecciona</option>
                        <option value="m">Masculino</option>
                        <option value="f">Femenino</option>
                        <option value="o">Otro</option>                 
                    </select>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="FECHA_N">FECHA DE NACIMIENTO</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="date" name="FECHA_N" id="FECHA_N" maxlength="50" class="form-control" placeholder="DD/MM/AAAA" required>
                </div>
                <div class="col-md-3" style="padding-left: 10px; padding-right: 0px;">
                    <label for="LUGAR_N">LUGAR DE NACIMIENTO</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="LUGAR_N" id="LUGAR_N" maxlength="50" class="form-control" pattern="[A-Za-z������������\s]+" placeholder="Mi Casa" required>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="DIRECCION">DIRECCI�N</label>
                </div>
                <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" id="DIRECCION" name="DIRECCION" maxlength="50" class="form-control" placeholder="Av. Siempreviva" required>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="TELEFONO">TEL�FONO</label>                        
                </div>
                <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                    <input type="number" id="TELEFONO" name="TELEFONO" maxlength="50" class="form-control" placeholder="555 5555/3000000000" required>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="EMAIL">E-MAIL</label>                        
                </div>
                <div class="col-md-6" style="padding-left: 0px; padding-right: 0px;">
                    <input type="email" id="EMAIL" name="EMAIL" maxlength="50" class="form-control" placeholder="xxx@yyy.zz">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <label for="SALARIO">SALARIO</label>                        
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="number" id="SALARIO" name="SALARIO" maxlength="50" class="form-control" placeholder="1000" required>
                </div>
                <div class="col-md-1" style="padding-left: 10px; padding-right: 0px;">
                    <label for="ACTIVO">ACTIVO</label>                            
                </div>
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <select name="ACTIVO" class="form-control" required>
                        <option value="">Selecciona</option>
                        <option value="S">S�</option>
                        <option value="N">No</option>
                    </select>
                </div>
            </div>

        </div>                
        <div class="clearfix"> </div>
        <hr style="border-top: 1px solid gray;">
        <%// ac� van los botones%>
        <div style="text-align: center;">
            <button style="margin: 10px" class="btn btn-danger" type="reset">Cancelar</button>
            <a style="margin: 10px"  id="buscar">Listado de Empleados</a>
            <button style="margin: 10px" class="btn btn-success" type="submit" >Guardar</button>
        </div>
    </FORM>
</div>
<br/>
<div id="mostrar" class="well form-group block-center container-fluid text-justify img-responsive" style="margin: 0 auto 0; max-width: 100%; max-height: 80%; " >
</div>

<script>
    function myFunction() {
        //document.getElementById("field2").value = document.getElementById("field1").value;
        sql = "execute ACTUALIZAR_EMAIL";
        Conexion.actualizar(sql);//ERRORRRRRRRRRRR
    }
    $(document).ready(function(){
        $("#buscar").click(function(){
            $("#mostrar").show;           
	});
	$("#buscar").click(function(){
            $.get("Index2_parte.jsp", {tabla: "EMPLEADOS"}, function(htmlexterno){
                $("#mostrar").html(htmlexterno);
            });            
	});
    });
</script>