<%//@page import="java.util.List"%>
<%//@page import="java.util.ArrayList"%>
<%//@page import="java.sql.*"%>
<%//@page import="Principal.Conexion"%>
<%@page import="Principal.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href="Templates/signin.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Form</title>
    </head>
    
    <body>    
        <jsp:include page="Templates/header.jsp"></jsp:include>  
        <H2 style="text-align: center;">DATOS PERSONALES</H2>
        <div class="form-group block-center container-fluid text-center" style="margin: 0 auto 0; max-width: 50%;" >
            <FORM METHOD="POST" ACTION="Confirmacion.jsp" >
                <div>
                <label for="identificacion" class="col-md-3">Identificación</label>
                    <input type="number" name="identificaion" id="identificacion" size=26 class="form-control" placeholder="1111111" required autofocus></p>
                </div>
                <div>
                <label for="nombres" class="col-md-3">Nombres</label>
                    <input type="text" name="nombres" id="nombres" size=15 pattern="[A-Za-z]+" class="form-control" placeholder="Anacleto" required></p>
                </div>
                <div >
                <label for="apellidos" class="col-md-3">Apellidos</label>
                    <input type="text" name="apellidos" id="apellidos" size=15 pattern="[A-Za-z]+" class="form-control" placeholder="Eustaquio" required></p>
                </div>
                <div>
                    <label for="genero" class="col-md-3">Género</label>
                    <!--
                    <input list="genero" name="genero" class="form-control" required placeholder="" >                                                  
                        <datalist id="genero">
                            <option value="M"> M </option>    
                            <option value="F"> F </option> 
                            <option value="O"> O </option> 
                        </datalist> 
                    -->
                    <select name="genero" class="form-control" required>
                        <option value="">Selecciona</option>
                        <option value="M">M</option>
                        <option value="F">F</option>
                        <option value="O">O</option>                 
                    </select>
                </div>
                <br/>
                <div>
                <label for="nacimiento" class="col-md-4">Fecha de Nacimiento</label>
                    <input type="date" name="nacimiento" id="nacimiento" size=26 class="form-control" placeholder="DD/MM/AAAA" required></p>
                </div>
                <div>
                <label for="direccion" class="col-md-3">Dirección</label>
                    <input type="text" id="direccion" name="direccion" size=26 class="form-control" placeholder="Av. Siempreviva" required>
                </div>
                <div>
                <label for="movil" class="col-md-3">Móvil</label>
                    <input type="number" id="movil" name="movil" size=26 class="form-control" placeholder="555-5555" required>
                </div>
                <div>
                <label for="email" class="col-md-3">E-Mail</label>
                    <input type="email" id="email" name="email" size=26 class="form-control" placeholder="xxx@yyy.zz" required>
                </div>
                <div>
                    <label for="perfil" class="col-md-3">Perfil Laboral</label>
                    <br/>
                    <textarea name="perfil" id="perfil" rows="10" cols="30" maxlength="200" placeholder="Cuéntanos... (Max. 200 Caracteres)" required></textarea>
                </div>
                
                <%// acá van los botones%>
                <div class="col-md-12" style="margin-top: 10px;">
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                    <button class="btn btn-success" type="submit">Enviar</button>
                </div>
            </FORM>
        </div>
        <jsp:include page="Templates/footer.jsp"></jsp:include> 
    </body>
</html>
