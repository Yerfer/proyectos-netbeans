<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Principal.Conexion"%>
<html lang="ES">
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href='//fonts.googleapis.com/css?family=Aclonica' rel='stylesheet'>
        <!--<script src="js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jquery.min.js" type="text/javascript"></script>-->
    </head>
    <body class="img-responsive" style="background-image: linear-gradient(180deg, white, #ABDFF9); color: #000000; font-family: Aclonica , Helvetica, Comic Sans MS, Verdana, Arial; "> 
        <% 
            if (Conexion.existeConexionDB()==false) { 
                response.sendRedirect("LoginDB.jsp");     
            }
            if(request.getParameter("repite")!=null){
                if(request.getParameter("repite").equalsIgnoreCase("true")){
                %> 
                    <script>alert("NO SE PUEDE AGREGAR VALORES REPETIDOS EN LAS PK");</script> 
                <% 
                }
            }
            if(request.getParameter("confirma")!=null){
                if(request.getParameter("confirma").equalsIgnoreCase("true")){
                %> 
                    <script>alert("Inserción Exitosa!!!");</script> 
                <% 
                }
            } 
        %>        
        <jsp:include page="Templates2017/Header.jsp"></jsp:include>
        <div class="container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#inicio">Inicio</a></li>
            <li><a data-toggle="tab" href="#empleados">Empleados</a></li>
            <li><a data-toggle="tab" href="#elementos">Elementos de Protección</a></li>
            <li><a data-toggle="tab" href="#entrada_almacen">Entrada de elementos</a></li>
            <li><a data-toggle="tab" href="#informes">Informes</a></li>
            <li><a data-toggle="tab" href="#procedimiento">Procedimiento PLSQL</a></li>
            <li><a data-toggle="tab" href="#funcion">Función PLSQL</a></li>
            <li class="nav navbar-nav navbar-right"><a href="Logout.jsp"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
          </ul>

          <div class="tab-content">
            <div id="inicio" class="tab-pane fade in active">
              <h3>HOME</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <div class="col-md-12" style="padding-right: 0px; padding-left: 0px; margin-left: 0%;">
                    <a href="http://www.google.com.co/" title="Autor Yerfer" target="_blank">
                        <img class="img-thumbnail img-rounded" src="Images/insecto.jpg" alt="Imagen de YERSON" style="width: 600px; height: 445px; margin-left: 14%; background-size: 100% 100%;"/>
                    </a> 
                </div>
            </div>
            <div id="empleados" class="tab-pane fade">
                <jsp:include page="FormularioEmpleado.jsp"></jsp:include>                
            </div>
            <div id="elementos" class="tab-pane fade">
                <jsp:include page="FormularioElemento.jsp"></jsp:include>
            </div>
            <div id="entrada_almacen" class="tab-pane fade">
                <jsp:include page="EntradaElementos.jsp"></jsp:include> 
            </div>
            <div id="informes" class="tab-pane fade">
                <jsp:include page="Consultas.jsp"></jsp:include>
            </div>
            <div id="procedimiento" class="tab-pane fade">
                <jsp:include page="Procedimiento.jsp"></jsp:include>
            </div>
            <div id="funcion" class="tab-pane fade">
                <jsp:include page="Funcion.jsp"></jsp:include>
            </div>
          </div>
        </div>
    <br/>
    <jsp:include page="Templates2017/Footer.jsp"></jsp:include> 
    </body>
</html>
