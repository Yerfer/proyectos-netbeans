<%@page import="Principal.Conexion"%>
<%@page import="java.sql.ResultSet"%>
<div class="well form-group block-center container-fluid text-justify img-responsive" id="divEntradaAlmacen" style="margin: 0 auto 0; max-width: 80%; max-height: 80%; " >
    <!--<FORM METHOD="POST" ACTION="Agregar2.jsp" >
        <input type="hidden" name="tabla" value="ELEMENTOS" />
        <input type="hidden" name="CODIGO" value="-1" />-->
        <div class="col-md-12"> 
            <div class="form-group col-md-12" style="">
                <div class="col-md-2" style="margin-left: 15%;">                    
                    <label>RESPONSABLE</label>   
                </div>
                <div class="col-md-8" style="">
                    <%  
                        String nombreEmpleado = "vacio";
                        String idEmpleado = request.getParameter("empleadoID");
                        String sql = "select NOM_EMPLEADO("+idEmpleado+") from EMPLEADOS e";
                        ResultSet resultado = Conexion.consultaDB(sql);
                        if (resultado.next()) {
                            nombreEmpleado = resultado.getString(1);
                        }
                        
                    %>
                    <label style="font-size: 20px;"> <%= nombreEmpleado.toUpperCase() %> </label> 
                    <input type="hidden" id="idEmpleado" value="<%= request.getParameter("empleadoID") %>"/>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-2">
                    <label for="fechaEntradaInput">Fecha </label>
                </div>
                <div class="col-md-3">
                    <input class="form-control" type="date" name="fechaEntradaInput" id="fechaEntradaInput" placeholder="FECHA"/>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-2">
                    <label for="conceptoEntradaInput">Concepto </label>
                </div>
                <div class="col-md-12">
                    <input class="form-control" type="text" name="conceptoEntradaInput" id="conceptoEntradaInput" placeholder="CONCEPTO"/>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-6">                  
                    <div class="col-md-4">
                        <label for="subtotalEntradaInput">Subtotal </label>
                    </div>
                    <div class="col-md-6">
                        <input class="form-control" type="number" name="subtotalEntradaInput" id="subtotalEntradaInput" placeholder="SUBTOTAL"/>
                    </div>                    
                </div>
                <div class="col-md-6">
                    <div class="col-md-4">
                        <label for="ivaEntradaInput">IVA </label>
                    </div>
                    <div class="col-md-6">
                        <input class="form-control" type="number" name="ivaEntradaInput" id="ivaEntradaInput" placeholder="IVA"/>
                    </div>                    
                </div>                
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-2">
                    <label for="totalEntradaInput">TOTAL </label>
                </div>
                <div class="col-md-8">
                    <input class="form-control" type="number" name="totalEntradaInput" id="totalEntradaInput" placeholder="TOTAL"/>
                </div>                
            </div>                        
        </div>
                
        <div class="clearfix"> </div>
        <hr style="border-top: 1px solid gray;">
        <%// ac� van los botones%>
        <div style="text-align: center;">
            <button style="margin: 10px" class="btn btn-info" id="verdetalleEntrada">Ver Detalle</button>
            <button style="margin: 10px" class="btn btn-danger" id="cancelarEntrada">Cancelar</button>
            <button style="margin: 10px" class="btn btn-success" id="ingresarEntrada">Ingresar</button>
        </div>
    <!--</FORM>-->
    
</div>
        
<!--<div id="cargadetalle"> </div>-->
        
<script>
    
    $(document).ready(function(){       
        $("#ingresarEntrada").click(function(){
            if ($("#fechaEntradaInput").val()===""||$("#conceptoEntradaInput").val()===""
                    ||$("#subtotalEntradaInput").val()===""||$("#ivaEntradaInput").val()===""||
                    $("#totalEntradaInput").val()==="") { 
                alert("LLENE TODOS LOS CAMPOS!!!");
            }else{
                $.post("Agregar2.jsp",{tabla:"ENTRADA_ALMACEN",ID_ENTRADA:"-1",FECHA:$("#fechaEntradaInput").val(),
                    RESPONSABLE:$("#idEmpleado").val(),CONCEPTO:$("#conceptoEntradaInput").val(),
                    SUBTOTAL:$("#subtotalEntradaInput").val(),IVA:$("#ivaEntradaInput").val(),TOTAL:$("#totalEntradaInput").val()
                });    
                alert("Inserci�n Exitosa!");
                $("#cargaEntradaAlmacen").hide(3000);
                $.post("FormularioDetalleEntrada.jsp", {idEmpleado:$("#empleadoID").val()}, function(htmlexterno){ 
                    $("#cargaDetalleEntrada").html(htmlexterno);
                });
                $("#cargaDetalleEntrada").show(3000);
            }

            $.post("VerDetalle.jsp",{},function(htmlexterno2){
                $("#cargadetalle").html(htmlexterno2);
            });
        });
	$("#cancelarEntrada").click(function(){
            $("#divEntradaAlmacen").hide(3000);
            $("#divEntradaAlmacen").hide("slow");
            $("#divResponsable").show(3000);
            $("#divResponsable").show("slow");
        });
        $.post("VerDetalle.jsp",{},function(htmlexterno2){
                $("#cargadetalle").html(htmlexterno2);
        });
        $("#verdetalleEntrada").click(function(){
            $.post("VerDetalle.jsp",{},function(htmlexterno2){
                $("#cargadetalle").html(htmlexterno2);
            });
        });
    });
</script>