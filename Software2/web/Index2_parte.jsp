<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
                <div class="col-md-12">                       
                    <%
                        if (request.getParameter("tabla")==null && request.getParameter("consulta")==null){   //if (request.getParameter("tabla")==null ){  
                            response.sendRedirect("Home.jsp"); 
                        }else{
                            String consultaSQL="";
                            String titulo="";
                            if (request.getParameter("consulta")==null) {
                                consultaSQL = "Select * from "+request.getParameter("tabla").toUpperCase()+"";
                                titulo = "LISTADO DE "+request.getParameter("tabla").toUpperCase();
                            }else if(request.getParameter("tabla")==null){
                                consultaSQL = request.getParameter("consulta").toUpperCase();
                                titulo = "CONSULTA N° "+request.getParameter("indice");
                            }
                            //System.out.println("SQL: \n"+consultaSQL);
                            ResultSet resultado = Conexion.consultaDB(consultaSQL);
                            if(resultado==null){
                                response.sendRedirect("Home.jsp");
                            }else{
                            %> 
                            <div style="text-align: center; text-shadow: 2px 2px 5px BLACK; color:white; margin: 24px; ">
                                
                                <h2><%= titulo %></h2>
                                
                            </div>                              
                            <div class="table block-center container-fluid text-justify img-responsive" style="overflow-x:auto; margin: 0 auto 0; max-width: 100%;" >
                                <table class="table table-hover table-condensed" style="margin: 0 auto 0; width: 99%; height: 100%; "> 
                                    <% 
                                    int nColumnas = resultado.getMetaData().getColumnCount();
                                    %>                                
                                    <thead>
                                        <tr>
                                            <%
                                            for (int i = 1; i <= nColumnas; i++) {
                                                %>
                                                    <th><%= resultado.getMetaData().getColumnLabel(i) %></th>                             
                                                <%
                                            }
                                            if(request.getParameter("tabla")!=null){
                                            %>
                                                <th>                                                
                                                    <!--<form action="Home.jsp#<%= request.getParameter("tabla").toLowerCase() %>" method="POST">-->
                                                    <form action="Home.jsp" method="POST">
                                                        <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                        <input href="FormularioEmpleado.jsp" class="btn btn-success btn-xs" type="submit" value="Agregar">
                                                    </form>
                                                </th>
                                                <%
                                            }
                                            %>
                                        </tr>
                                    </thead>
                                    <% 
                                    while(resultado.next()){                             
                                        %><tr><%
                                        for (int i = 1; i <= nColumnas; i++) {
                                            %>
                                                <td><%=resultado.getString(i)%></td>                                             
                                            <%
                                        }
                                        %>
                                        <td>
                                            <div class="btn-group btn-group-xs">
                                                <!-- Aquí van los botones Editar y Elimnar de cada FILA -->
                                            </div>
                                        </td>
                                        </tr> 
                                        <%
                                    }
                                %> 
                                </table> 
                            </div>
                            <%
                            }
                        }
                    
                    %>                    
                </div>