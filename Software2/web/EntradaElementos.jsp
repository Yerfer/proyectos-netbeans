<%@page import="java.sql.ResultSet"%>
<%@page import="Principal.Conexion"%>
<div style="text-align: center; text-shadow: 2px 2px 5px BLACK; color:white; margin: 24px; ">
    <h3>FORMULARIO PARA EL REGISTRO DE INGRESO DE ELEMENTOS DE PROTECCI�N</h3>
</div>

<div class="well form-group block-center container-fluid text-justify img-responsive" id="divResponsable" style="margin: 0 auto 0; max-width: 80%; max-height: 80%; " >
    <!--<FORM METHOD="POST" ACTION="Agregar2.jsp" >-->
        <input type="hidden" name="tabla" value="ELEMENTOS" />
        <input type="hidden" name="CODIGO" value="-1" />
        <div class="col-md-12"> 
            <div class="form-group col-md-12">
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">                    
                    <label  for="RESPONSABLE">RESPONSABLE</label>   
                </div>
                <div class="col-md-10" style="padding-left: 0px; padding-right: 0px;">
                    <select name="empleadoID" id="empleadoID" class="form-control" required>
                            <option value="">-- Responsable --</option>
                            <%
                                String sql = "select e.IDENTIFICACION, NOM_EMPLEADO(e.IDENTIFICACION) from EMPLEADOS e";
                                ResultSet resultado = Conexion.consultaDB(sql);
                                if(resultado==null){
                                    %>
                                        <option value="">Sin Datos</option>
                                    <%
                                }
                                while(resultado.next()){                                            
                                    %>
                                        <option value="<%= resultado.getString(1)  %>"><%= resultado.getString(2)  %></option>
                                    <%
                                }
                            %>
                            <!--<option value="1">nombre1</option>-->
                    </select>                
                </div>
            </div>            
        </div>
        <div class="clearfix"> </div>
        <hr style="border-top: 1px solid gray;">
        <%// ac� van los botones%>
        <div style="text-align: center;">
            <button style="margin: 10px" class="btn btn-info" id="verdetalle">Ver Detalle</button>
            <button style="margin: 10px" class="btn btn-danger" id="cancelar">Cancelar</button>
            <button style="margin: 10px" class="btn btn-success" id="ingresar">Ingresar</button>
        </div>
    <!--</FORM>-->
    
</div>
        <h2 id="prueba"></h2>        
<div id="cargaEntradaAlmacen"></div>
<div id="cargaDetalleEntrada"></div>        
<div id="cargadetalle"></div>
        
<script>
    $(document).ready(function(){
	$("#ingresar").click(function(){
            //$("#prueba").val("2");
            //alert($("#prueba").val());   //asigna un valor pero no lo cambia en el html.
            //var cosaX = "hola mundo";
            //$("#prueba").val(cosaX);
            //$("#prueba").html(cosaX);  //lo cambia en el html pero su valor sigue siendo el asignado por val().
            //alert($("#prueba").val());
            //alert($("#empleadoID").val());
            if ($("#empleadoID").val()==="") {
                alert("Seleccione un RESPONSABLE");
            }else{
                $("#divResponsable").hide(3000);
                //$("#divResponsable").hide("slow");
                $.post("FormularioEntradaAlmacen.jsp", {empleadoID:$("#empleadoID").val()}, function(htmlexterno){ 
                    $("#cargaEntradaAlmacen").html(htmlexterno);
                });
                $("#cargaEntradaAlmacen").show(3000);
            }            
        });
        $("#verdetalle").click(function(){
            $.post("VerDetalle.jsp",{},function(htmlexterno2){
                $("#cargadetalle").html(htmlexterno2);
            });
        });
        $("#cancelar").click(function(){
            $("#empleadoID").val("");
        });
    });
</script>