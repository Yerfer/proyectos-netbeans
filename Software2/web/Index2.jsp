<%@page import="java.sql.*"%><%@page import="Principal.Conexion"%><%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='//fonts.googleapis.com/css?family=Aclonica' rel='stylesheet'>
        <title>INDEX</title>
    </head>
    
    <body class="img-responsive" style="background-image: linear-gradient(180deg, #ABDFF9, #4BAEE1); color: #000000; font-family: Aclonica , Helvetica, Comic Sans MS, Verdana, Arial; "> 
        <jsp:include page="Templates2017/Header.jsp"></jsp:include>              
        <div class="container-fluid text-center">
            <div class="row content">
                <% 
                    if (Conexion.existeConexionDB()==false) { 
                        response.sendRedirect("LoginDB.jsp");   
                    }else{
                %> 
                <div class="col-md-12">                       
                    <%
                        if (request.getParameter("tabla")==null && request.getParameter("consulta")==null){   //if (request.getParameter("tabla")==null ){  
                            response.sendRedirect("Home.jsp"); 
                            System.out.println("IF1");
                        }else{
                            System.out.println("ELSE1");
                            String consultaSQL="";
                            String titulo="";
                            if (request.getParameter("consulta")==null) {
                                System.out.println("TABLA");
                                consultaSQL = "Select * from "+request.getParameter("tabla").toUpperCase()+"";
                                titulo = "LISTADO DE "+request.getParameter("tabla").toUpperCase();
                            }else if(request.getParameter("tabla")==null){
                                System.out.println("CONSULTA");
                                consultaSQL = request.getParameter("consulta").toUpperCase();
                                titulo = "CONSULTA N° "+request.getParameter("indice");
                            }
                            System.out.println("SQL: \n"+consultaSQL);
                            ResultSet resultado = Conexion.consultaDB(consultaSQL);
                            if(resultado==null){
                                response.sendRedirect("Home.jsp");
                            }else{
                            %> 
                            <div style="text-align: center; text-shadow: 2px 2px 5px BLACK; color:white; margin: 24px; ">
                                
                                <h2><%= titulo %></h2>
                                
                            </div>
                            <div class="img-rounded col-md-1" style="padding-left: 0px; padding-right: 0px; margin-top: 10px; margin-bottom: 10px;">
                                <input class="img-rounded" type="button" onclick="location.href='Home.jsp' " value="Regresar" name="boton" style="background-color: gray; "/> 
                            </div>                                
                            <div class="table block-center container-fluid text-justify img-responsive" style="overflow-x:auto; margin: 0 auto 0; max-width: 100%;" >
                                <table class="table table-striped" style="margin: 0 auto 0; width: 99%; height: 100%; "> 
                                    <% 
                                    int nColumnas = resultado.getMetaData().getColumnCount();
                                    %>                                
                                    <thead>
                                        <tr>
                                            <%
                                            for (int i = 1; i <= nColumnas; i++) {
                                                %>
                                                    <th><%= resultado.getMetaData().getColumnLabel(i) %></th>                             
                                                <%
                                            }
                                            if(request.getParameter("tabla")!=null){
                                            %>
                                                <th>                                                
                                                    <!--<form action="Home.jsp#<%= request.getParameter("tabla").toLowerCase() %>" method="POST">-->
                                                    <form action="Home.jsp" method="POST">
                                                        <input type="hidden" name="tabla" value="<%= request.getParameter("tabla") %>" />
                                                        <input href="FormularioEmpleado.jsp" class="btn btn-success btn-xs" type="submit" value="Agregar">
                                                    </form>
                                                </th>
                                                <%
                                            }
                                            %>
                                        </tr>
                                    </thead>
                                    <% 
                                    while(resultado.next()){                             
                                        %><tr><%
                                        for (int i = 1; i <= nColumnas; i++) {
                                            %>
                                                <td><%=resultado.getString(i)%></td>                                             
                                            <%
                                        }
                                        %>
                                        <td>
                                            <div class="btn-group btn-group-xs">
                                                <!-- Aquí van los botones Editar y Elimnar de cada FILA -->
                                            </div>
                                        </td>
                                        </tr> 
                                        <%
                                    }
                                %> 
                                </table> 
                            </div>
                            <%
                            }
                        }
                    }
                    %>                    
                </div>
            </div>
            <div class="img-rounded col-md-1" style="padding-left: 0px; padding-right: 0px; margin-top: 10px; margin-bottom: 10px;">
                <input class="img-rounded" type="button" onclick="location.href='Home.jsp' " value="Regresar" name="boton" style="background-color: gray; "/> 
            </div>
        </div>                
        <jsp:include page="Templates2017/Footer.jsp"></jsp:include>        
    </body>        
    
</html>
