<div style="text-align: center; text-shadow: 2px 2px 5px BLACK; color:white; margin: 24px; ">
    <h3>FORMULARIO PARA EL REGISTRO DE ELEMENTOS DE PROTECCI�N</h3>
</div>

<div class="well form-group block-center container-fluid text-justify img-responsive" style="margin: 0 auto 0; max-width: 80%; max-height: 80%; " >
    <FORM METHOD="POST" ACTION="Agregar2.jsp" >
        <input type="hidden" name="tabla" value="ELEMENTOS" />
        <input type="hidden" name="CODIGO" value="-1" />
        <div class="col-md-12"> 
            <div class="form-group col-md-12">
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <label  for="ELEMENTO">ELEMENTO</label>   
                </div>
                <div class="col-md-10" style="padding-left: 0px; padding-right: 0px;">
                    <input class="form-control" type="text" name="ELEMENTO" id="ELEMENTO" maxlength="50" pattern="[A-Za-z0-9������������\s]+" placeholder="ELEMENTO" required>
                </div>
            </div>                    
            <div class="col-md-12 form-group">
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <label for="DEVOLUTIVO">DEVOLUTIVO</label>
                </div>
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="DEVOLUTIVO" id="DEVOLUTIVO" maxlength="2" pattern="[A-Za-z0-9������������\s]+" class="form-control" placeholder="DEVOLUTIVO">
                </div>
                <div class="col-md-2" style="padding-left: 10px; padding-right: 0px;">
                    <label for="TALLA">TALLA</label>
                </div>
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="TALLA" id="TALLA" maxlength="2" pattern="[A-Za-z0-9������������\s]+" class="form-control" placeholder="TALLA">
                </div>
                <div class="col-md-1" style="padding-left: 10px; padding-right: 0px;">
                    <label for="USO">USO</label>
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="USO" id="USO" maxlength="12" pattern="[A-Za-z0-9������������\s]+" class="form-control" placeholder="USO">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <label for="MATERIALES">MATERIALES</label>
                </div>                        
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <textarea name="MATERIALES" id="MATERIALES" pattern="[A-Za-z������������\s]+" class="form-control" style="resize: vertical;" rows="4" cols="10" maxlength="400" placeholder="(Max. 400 Caracteres)"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <label for="MANTENIMIENTO">MANTENIMIENTO</label>
                </div>                        
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <textarea name="MANTENIMIENTO" id="MANTENIMIENTO" pattern="[A-Za-z������������\s]+" class="form-control" style="resize: vertical;" rows="4" cols="10" maxlength="300" placeholder="(Max. 300 Caracteres)"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <label for="USOS">USOS</label>
                </div>                        
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <textarea name="USOS" id="USOS" pattern="[A-Za-z������������\s]+" class="form-control" style="resize: vertical;" rows="4" cols="10" maxlength="300" placeholder="(Max. 300 Caracteres)"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <label for="NORMA">NORMA</label>
                </div>                        
                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                    <textarea name="NORMA" id="NORMA" pattern="[A-Za-z������������\s]+" class="form-control" style="resize: vertical;" rows="4" cols="10" maxlength="400" placeholder="(Max. 400 Caracteres)"></textarea>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <label for="ATENUACION">ATENUACI�N</label>
                </div>                        
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="ATENUACION" id="ATENUACION" maxlength="50" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="ATENUACION">
                </div>
                <div class="col-md-2" style="padding-left: 40px; padding-right: 0px;">
                    <label for="SERIAL">SERIAL</label>
                </div>                        
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" name="SERIAL" id="SERIAL" maxlength="250" pattern="[A-Za-z������������\s]+" class="form-control" placeholder="SERIAL" required>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                    <label for="TALLAS">TALLAS</label>                        
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="text" id="TALLAS" name="TALLAS" maxlength="250" class="form-control" placeholder="TALLAS">
                </div>
                <div class="col-md-2" style="padding-left: 40px; padding-right: 0px;">
                    <label for="UNIDAD">UNIDAD</label>                        
                </div>
                <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                    <input type="number" id="UNIDAD" name="UNIDAD" class="form-control" placeholder="UNIDAD">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                    <label for="RUTA">RUTA</label>                        
                </div>
                <div class="col-md-10" style="padding-left: 20px; padding-right: 0px;">
                    <input type="url" id="RUTA" name="RUTA" maxlength="50" class="form-control" placeholder="RUTA">
                </div>
            </div>
            <div class="col-md-12 form-group">
                <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                    <label for="PRECIO_ACTUAL">PRECIO</label>                        
                </div>
                <div class="col-md-4" style="padding-left: 20px; padding-right: 0px;">
                    <input type="number" id="PRECIO_ACTUAL" name="PRECIO_ACTUAL" class="form-control" placeholder="PRECIO_ACTUAL">
                </div>
                <div class="col-md-3" style="padding-left: 20px; padding-right: 0px;">
                    <label for="CANTIDAD_ELEMENTOS">CANTIDAD ELEMENTOS</label>                        
                </div>
                <div class="col-md-3" style="padding-left: 5px; padding-right: 0px;">
                    <input type="number" id="CANTIDAD_ELEMENTOS" name="CANTIDAD_ELEMENTOS" class="form-control" placeholder="CANTIDAD">
                </div>
            </div>
        </div>                
        <div class="clearfix"> </div>
        <hr style="border-top: 1px solid gray;">
        <%// ac� van los botones%>
        <div style="text-align: center;">
            <button style="margin: 10px" class="btn btn-danger" type="reset">Cancelar</button>
            <a style="margin: 10px" href="Index2.jsp?tabla=ELEMENTOS">Listado de Elementos</a>
            <button style="margin: 10px" class="btn btn-success" type="submit" >Guardar</button>
        </div>
    </FORM>
</div>