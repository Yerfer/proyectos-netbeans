<%@page import="Principal.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link href='//fonts.googleapis.com/css?family=Aclonica' rel='stylesheet'>
        <title>LOGIN</title>
    </head>
    
    <body class="img-responsive" style="background-image: linear-gradient(180deg, white, #ABDFF9); color: #000000; font-family: Aclonica , Helvetica, Comic Sans MS, Verdana, Arial; "> 
        
        <% 
        if (Conexion.existeConexionDB()==false) { 
        %>  
        <jsp:include page="Templates2017/Header.jsp"></jsp:include> 
        
        <div class="col-md-12" style="margin-bottom: 2%; margin-top: 2%; max-height: 100%;">
            <div class="col-md-8" style="">
                <img class="img-thumbnail img-rounded" src="Images/fcbi.jpg" alt="Imagen de FCBI" style="width: 600px; height: 445px; margin-left: 14%; background-size: 100% 100%;"/>
                <!--<div class="img-thumbnail" style="background-image: url(Images/fcbi.jpg); width: 600px; height: 445px; margin-left: 14%; background-size: 100% 100%;"></div>
                <div class="col-sm-2" style="width: 129px; height: 100px; background-image: url(Images/logo.png);background-size: 100% 100%; margin-top: 4%; margin-left: 3%;"></div>
           --> </div>
            
            <div class="col-md-4" style="">                
                <div class="col-md-12" style="margin-bottom: 10px"><!--padding-left: 0px; padding-right: 0px;-->                        
                    <!--<legend style="text-align: center;">Iniciar Sesión</legend>-->
                    <!--<div class="block-center col-md-12" style="" ><!--margin: 0 auto 0; max-width: 50%;-->
                    <FORM METHOD="POST" ACTION="Conexion.jsp" id="login" name="login">
                        <fieldset form="login" class="form-group">
                        <legend class="col-form-legend">Iniciar Sesión</legend>
                            <div class="form-group">
                                <label for="usuario" class="col-md-12" style="margin-top: 5px;">USUARIO</label>
                                <input type="text" name="usuario" id="usuario" size="26" class="col-md-9 form-control" placeholder="Usuario" required autofocus></p>
                            </div>
                            <div class="form-group">
                                <label for="clave" class="col-md-12" style="margin-top: 10px;">CONTRASEÑA</label>
                                <input type="password" id="clave" name="clave" size="26" class="col-md-9 form-control" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group">
                                <label for="cadConexion" class="col-md-12" style="margin-top: 10px;">CADENA DE CONEXIÓN</label>
                                <input type="text" name="cadConexion" id="cadConexion" size="10" class="col-md-9 form-control" placeholder="XE" required></p>
                            </div>
                            <div class="form-group col-md-12" style="margin-top: 28px;">
                                <button class="btn btn-success" type="submit" style="margin-right:  10%; margin-left: 20%;">ACEPTAR</button>
                                <button class="btn btn-danger" type="reset" style="margin-right:  0px;">CANCELAR</button>
                            </div>
                        </fieldset>
                    </FORM>
                    <!--</div>-->
                </div>
                
                
                <div class="col-md-12" style="height:  30px;">
                        <!--<a href="/www.facebook.com" target="_blank">
                        <img src="Images/fb.png" style="height: 100%; width: 20px; background-size: 100% 100%;">
                        <div class="caption">-->
                        <div class="col-md-3" style="padding-right: 0px; padding-left: 0px; margin-left:36%; width: 40px;">
                            <a href="http://www.unillanos.edu.co/" title="Unillanos" target="_blank">
                                <img class="" src="Images/web3.png" alt="Imagen de Unillanos" style="width: 35px; height: 35px; margin-left: 2px; background-size: 100% 100%;"/>
                            </a> 
                        </div>
                        <div class="col-md-3" style="padding-right: 0px; padding-left: 0px; width: 40px;">
                            <a href="https://www.facebook.com/UnillanosOficial/" title="Facebook" target="_blank">
                                <img class="" src="Images/fb.png" alt="Imagen de FB" style="width: 35px; height: 35px; margin-left: 2px; background-size: 100% 100%;"/>
                            </a> 
                        </div>                        
                        <div class="col-md-3" style="padding-right: 0px; padding-left: 0px; width: 40px;">
                            <a href="https://twitter.com/unillanos_?original_referer=http%3A%2F%2Fwww.unillanos.edu.co%2F&profile_id=186113829&tw_i=587459558714568704&tw_p=embeddedtimeline&tw_w=461521937642967040/" title="Twitter" target="_blank">
                                <img class="" src="Images/tw2.png" alt="Imagen de Twitter" style="width: 35px; height: 35px; margin-left: 2px; background-size: 100% 100%;"/>
                            </a> 
                        </div>                        
                        <!--<div class="img-thumbnail" alt="Facebook" style="background-image: url(Images/fb.png); width: 50px; height: 50px; margin-left: 14%; background-size: 100% 100%;"></div>
                        -->
                </div>
            </div>           
        </div>
                            
            <% 
        } else { 
            %>
            <P><B>Sesión DB iniciada</B>:</P>
            <%
                //response.sendRedirect("Index.jsp"); 
                response.sendRedirect("Home.jsp");
        } 
        %>
    <jsp:include page="Templates2017/Footer.jsp"></jsp:include>  
    </BODY>
</html>
