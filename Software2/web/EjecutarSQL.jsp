<%@page import="java.sql.Types"%>
<%@page import="java.sql.CallableStatement"%>
<%@page import="Principal.Conexion"%>
<div>
    <%
        CallableStatement cs;
        if (Integer.parseInt(request.getParameter("funcion"))==1) {
            
            %>
            <div>
                <h3>Valor Promedio: </h3>
            </div>
            <div>
                <% 
                    String consulta="";
                    if (Integer.parseInt(request.getParameter("opcionf"))==1) {
                        //OPCI�N 1 DE EJECUCI�N DE LA FUNCI�N 
                        //Ejecuta la funci�n a trav�s de un select.
                        String sql = "select promedioElemento("+request.getParameter("elementoID")+") from dual";
                        consulta = Conexion.ejecutarFuncion1(sql);
                    }else{
                        //OPCI�N 2 DE EJECUCI�N DE LA FUNCI�N
                        cs = Conexion.getConexion().prepareCall("{? = call promedioElemento (?)}");
                        //CallableStatement cs = Conexion.getConexion().prepareCall("begin ? := promedioElemento(?); end;");
                        cs.registerOutParameter(1,Types.NUMERIC);   
                        cs.setString(2, request.getParameter("elementoID"));
                        cs.executeUpdate();
                        consulta = cs.getString(1);
                        cs.close();
                    }
                    if(consulta==null){
                            consulta="false";
                    }
                    if (consulta.equalsIgnoreCase("false")) {
                        %>
                        <h2>ELEMENTO NO ENCONTRADO.</h2>
                        <%
                    }else{
                        %>
                        <h2><%= consulta %></h2>
                        <%
                    }
                    consulta="";
                %>
            </div>
            <%  
        }
        if(Integer.parseInt(request.getParameter("funcion"))==0){
            //Ejecutar el proc opci�n 1:
            if(Integer.parseInt(request.getParameter("opcionp"))==1){
                //CallableStatement cs = Conexion.getConexion().prepareCall("begin ACTUALIZAR_EMAIL; end;");
                String x = "begin ACTUALIZAR_EMAIL; end;";
                Conexion.ejecutar(x);
                x="";
            }else{ //Ejecutar el proc opci�n 2:
                cs = Conexion.getConexion().prepareCall("{call ACTUALIZAR_EMAIL}");
                cs.executeUpdate();
                cs.close();
            }
            
        }
    %>
</div>