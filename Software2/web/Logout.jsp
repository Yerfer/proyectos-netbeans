<%@page import="Principal.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body>
        <% if (Conexion.existeConexionDB()==true) { 
                Conexion.cerrarConexionDB();
                %>
                <P><B>Sesión DB CERRADA</B>:
                <%
            } 
            response.sendRedirect("LoginDB.jsp"); 
        %>
    </body>
</html>
