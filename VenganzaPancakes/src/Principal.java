/**
 *
 * @author Yerson Porras
 */

import java.util.Scanner;

public class Principal {
    
    Scanner leer = new Scanner(System.in);
    private int nCasos;
    
    public void pedir(){
        nCasos=leer.nextInt();
        for (int i = 0; i < nCasos; i++) {
            String pilaTortas = leer.next();
            int cambios = 0;
            char[] cadenaCaso = pilaTortas.toCharArray();
            int[] cadenaCasoInt = new int[cadenaCaso.length];
            for (int j = 0; j < cadenaCaso.length; j++) {
                cadenaCasoInt[j] = Integer.parseInt(cadenaCaso[j]+"1");
            }
            int rta = operar(cadenaCasoInt,cambios);
            System.out.println("Case #"+(i+1)+": "+rta);
        }
    }
    
    public int operar(int[] cadenaCasoInt, int cambios){
        /*for (int k = 0; k < cadenaCasoInt.length; k++) {
            System.out.print(cadenaCasoInt[k]+" |");
        }
        System.out.println("");*/
        for (int i = cadenaCasoInt.length-1; i >= 0; i--) { //de dere a izq buscando el (-).
            if (cadenaCasoInt[i] == -1) {
                int temp = i;
                if(cadenaCasoInt[0]== -1){  //comparamos el primer valor y preguntamos.
                    for (int j = 0; j <= i; j++) {
                        if(j>temp){
                            break;
                        }else{
                            int aux = cadenaCasoInt[temp];
                            cadenaCasoInt[temp] = cadenaCasoInt[j]*(-1);
                            cadenaCasoInt[j] = aux*(-1);                            
                            temp--;                            
                        }                        
                    }
                    cambios++;
                    return operar(cadenaCasoInt,cambios);
                }
                else{
                    int aux = 0;
                    do{
                        cadenaCasoInt[aux]=-1;
                        aux++;
                    }while(cadenaCasoInt[aux]==1); //de izq a dere 
                    cambios++;                    
                    return operar(cadenaCasoInt,cambios);
                }
            }
        }        
        return cambios;
    }
    
    
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.pedir();
    }
}
