import java.util.Scanner;

public class Principal {
    
    
    Scanner leer = new Scanner(System.in);
    private int n;
    private String cosa[];
    
    public void pedir(){
        n=leer.nextInt();
        cosa = new String[n];
        for (int i = 0; i < n; i++) {
            cosa[i]=leer.next();
        }
    }
    
    public void mostrar(){
        for (int i = 0; i < n; i++) {
            System.out.print(cosa[i]+"\n");
        }
    }
    
    
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.pedir();
        obj.mostrar();       
        
    }
    
    //no no no: sin paquete y se compila y ejecuta con el .class.
    
    // java Principal < in.in > out.out
    
    
    
}

