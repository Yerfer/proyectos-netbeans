
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author Yerson Porras
 */
public class Principal {
        
    private int N = 6;
    private int J = 3;
    private int[] coinJam = new int[N];
    private int[] aux = new int[N];
    java.math.BigInteger divisor = new java.math.BigInteger(""+0);
       
    public void iniciar(){
        for (int i = 0; i < this.N; i++) {
            if(i==0||i==(this.N-1)){
                this.coinJam[i]=1;
            }
            else{
                this.coinJam[i]=0;
            }
        }
        System.out.print("Case #1:\n");
        for (int i = 0; i < this.J; i++) {
            
            //System.out.println("Vamos a operar");
            //for (int j = 0; j < this.coinJam.length; j++) {
            //    System.out.print(this.coinJam[j]+" |");
            //}
            //System.out.println("");
            
            operar(this.coinJam);
            
            //System.out.println("i = "+i);
            int[] auxiliar = incrementar(unificar10(this.coinJam));
            //System.out.println("hay aumento");
            //System.out.println("Aux");
            //for (int j = 0; j < auxiliar.length; j++) {
            //    System.out.print(auxiliar[j]+" |");
            //}
            System.out.println("\np");
            //this.coinJam = incrementar(unificar10(this.coinJam));
            for (int j = 0; j < this.coinJam.length; j++) {
                this.coinJam[j]=auxiliar[j];
            }
            //System.out.println("Despues de copiar");
            //for (int j = 0; j < this.coinJam.length; j++) {
            //    System.out.print(this.coinJam[j]+" |");
            //}
            //System.out.println("");
            //System.arraycopy(incrementar(unificar10(this.coinJam)),0,this.aux,0,this.coinJam.length);
            //for (int j = 0; j < this.aux.length; j++) {
            //    System.out.print(this.aux[i]+" |");
            //}
            System.out.println("q");
            //System.arraycopy(this.aux,0,(this.coinJam),0,this.coinJam.length);
        }
    }
    
    public void operar(int[] coinjam){
        //System.out.println("Operar con: ");
        //for (int i = 0; i < coinjam.length; i++) {
         //   System.out.print(coinjam[i]+" |");
        //}
        //System.out.println("");
        java.math.BigInteger rtaBase10 = new java.math.BigInteger(""+0);
        java.math.BigInteger[] divisoresCoinjam = new java.math.BigInteger[9]; //divisores de posi: 0 - 8. // en veremos: Coinjam en posi: 9;  
        rtaBase10 = unificar10(coinjam);
        //System.out.println("Base 10 "+rtaBase10);
        //if (esPrimo(rtaBase10)) {
        //   operar(incrementar(rtaBase10));
        //}else{
        //    divisoresCoinjam[8] = this.divisor;
            //System.out.println("Divisor: "+divisoresCoinjam[8]);
       // }
            for (int i = 2; i < 11; i++) {
                java.math.BigInteger rtaBaseDeI = convertir(rtaBase10,i);
                //System.out.println("Base "+i+" :"+rtaBase10);
                //System.out.println("Base 10: "+rtaBaseDeI);
                if (esPrimo(rtaBaseDeI)) {
                    operar(incrementar(rtaBase10));
                }
                else{
                    divisoresCoinjam[(i-2)] = this.divisor;
                }
            }
        //}
        //necesito retornar el coinjam y sus divisores.
        //divisoresCoinjam[9] = rtaBase10;
        System.out.print(rtaBase10+" ");
        for (int i = 0; i < 9; i++) {
            System.out.print(divisoresCoinjam[i]+" ");
        }
        System.out.println("hola");
        //return divisoresCoinjam;
    }
    
    
    public java.math.BigInteger convertir(java.math.BigInteger coinjam, int base){        
        java.math.BigInteger rta = new java.math.BigInteger(""+Integer.parseInt(""+coinjam,base));         
        //System.out.println("De base "+base+" a base 10: "+rta);
        return rta;
    }
    
    //public int[] incrementar(int[] coinjam){
    public int[] incrementar(java.math.BigInteger coinjam){
        //System.out.println("Llega: "+coinjam);
        java.math.BigInteger valorBase2 = new java.math.BigInteger(""+Integer.parseInt(""+coinjam,2));
        //.add(java.math.BigInteger.valueOf(1))
        char[] coinjamChar = ( valorBase2.add(java.math.BigInteger.valueOf(1)).toString(2) ).toCharArray(); // llega (10) y +1 y luego a (2)
        int [] coinjamInt = new int[coinjamChar.length];
        for (int i = 0; i < coinjamChar.length; i++) {
            coinjamInt[i]=Integer.parseInt(""+coinjamChar[i]);
        }
        //System.out.println("Aumentó en 1: ");
        //for (int i = 0; i < coinjamInt.length; i++) {
         //   System.out.print(coinjamInt[i]+" |");
        //}
        //System.out.println("");
        if (coinjamInt[coinjamInt.length-1]==0) {// || coinjamInt[0]==0) {
            //System.out.println("el último digito es 0");
            return incrementar(unificar10(coinjamInt));
        }
        //System.out.println("Regresará: ");
        //for (int i = 0; i < coinjamInt.length; i++) {
        //    System.out.print(coinjamInt[i]+" |");
        //}
        //System.out.println("");
        return coinjamInt;
    }
    
    public boolean esPrimo(java.math.BigInteger valor){
        int cont = 0;
        this.divisor = java.math.BigInteger.valueOf(0);
        //java.math.BigInteger rtaAcu = new java.math.BigInteger(""+valor); 
        //java.math.BigInteger i = new java.math.BigInteger("1"); 
        /*for(int i=1;(rtaAcu.compareTo(java.math.BigInteger.valueOf(i)))>0;i=i.add(java.math.BigInteger.valueOf(1))){  
            if( rtaAcu.mod(java.math.BigInteger.valueOf(i)).compareTo(java.math.BigInteger.valueOf(0)) == 0 ){                 
                cont++;  
                
                if ( cont==2 ) {
                    this.divisor = 
                }
            }  
        }*/
        java.math.BigInteger i = new java.math.BigInteger(""+1); 
        do{
            if( valor.mod(i).compareTo(java.math.BigInteger.valueOf(0)) == 0 ){                 
                cont++;                
                if ( cont==2 ) {                    
                    this.divisor = i;
                    //System.out.println("Segundo divisor: "+this.divisor);
                }
            }
            //System.out.println("i: "+i);
            i=i.add(java.math.BigInteger.valueOf(1));
        }while((valor.compareTo(i))>0);
        //System.out.println("Cont: "+cont);
        if(cont >= 2){
            //System.out.println("NO es primo ");
            return false;
        }else{
            //System.out.println("SÍ es primo ");
            return true;   
        } 
             
    }
    
    public java.math.BigInteger unificar10(int[] vector){
        /*System.out.println("En unificar");
        for (int i = 0; i < vector.length; i++) {
            System.out.print(vector[i]+" |");
        }
        System.out.println("");*/
        int cont=0;
        java.math.BigInteger rtaAcu = new java.math.BigInteger(""+0);
        for (int i = vector.length-1; i >= 0; i--) {   //inicia de dere a izq desde la n-1 posiciones
            BigInteger valor = new BigDecimal(Math.pow(10, cont)).toBigInteger();
            cont++;
            rtaAcu = rtaAcu.add( java.math.BigInteger.valueOf(vector[i]).multiply(valor) );           //pq el coinjam inicia (der->izq) con un 1.
        }
        //System.out.println("Luego de unir");
        //System.out.println(rtaAcu);
        return rtaAcu;
    }
    
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.iniciar();
    }
    
}
