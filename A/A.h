/* 
 * File:   A.h
 * Author: Luz Mery
 *
 * Created on 30 de abril de 2016, 01:55 PM
 */

#ifndef A_H
#define	A_H

class A {
public:
    A();
    A(const A& orig);
    virtual ~A();
private:

};

#endif	/* A_H */

