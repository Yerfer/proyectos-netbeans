/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadena_de_markov;

import java.util.ArrayList;
import java.lang.Math.*;
import static java.lang.Math.pow;

/**
 *
 * @author Yerson Porras.
 */
public class Cadena_de_Markov {
    double x = pow(2,2);
    private ArrayList p1 = new ArrayList();
    private ArrayList p2 = new ArrayList();
    private ArrayList p3 = new ArrayList();
    private double[][] piT = { {(4.0/7.0),(1.0/3.0),(4.0/8.0)}, {(2.0/7.0),(1.0/3.0),(3.0/8.0)}, {(1.0/7.0),(1.0/3.0),(1.0/8.0)} };
    private int n = -1;
    private double acu=0, rangoError;
    ArrayList<ArrayList<Double>> array = new ArrayList<ArrayList<Double>>();

    public Cadena_de_Markov(double x,double y,double z, double error) {
        this.p1.add(x);
        this.p2.add(y);
        this.p3.add(z);
        this.array.add(new ArrayList<Double>());
        this.array.get(0).add(x);
        this.array.get(0).add(y);
        this.array.get(0).add(z);
        this.rangoError = error;
        multiplicarMatrices(this.array);
    }

    public ArrayList<ArrayList<Double>> getArray() {
        return array;
    }
    
    
    
    //public void multiplicarMatrices(ArrayList p1, ArrayList p2, ArrayList p3){
public void multiplicarMatrices( ArrayList<ArrayList<Double>> array ){
++n;
this.array.add(new ArrayList<Double>());          

for (int i = 0; i < this.piT.length; i++) {
acu=0;
for (int j = 0; j < this.piT[0].length; j++) {
acu = acu + this.piT[i][j]*((double)array.get(n).get(j));
}
this.array.get(n+1).add(acu);
}

if(!esEstable(this.array)){
    multiplicarMatrices(this.array);
}
else{
    System.out.println(this.array);
}       

}
    
    public boolean esEstable(ArrayList<ArrayList<Double>> array ){
        //Teoría de Errores:
        int nTamano = array.size();
        int n = nTamano-1;        
        double error1 = ((double)array.get(n).get(0) - (double)array.get(n-1).get(0));
        //System.out.println("Error1 = "+error1);
        if(error1 > -rangoError && error1 < rangoError){
            double error2 = ((double)array.get(n).get(1) - (double)array.get(n-1).get(1));
            //System.out.println("Error2 = "+error2);
            if(error2 > -rangoError && error1 < rangoError){
                double error3 = ((double)array.get(n).get(2) - (double)array.get(n-1).get(2));
                //System.out.println("Error3 = "+error3);
                if(error3 > -rangoError && error1 < rangoError){
                    return true;
                }
            }
        }
        return false;  
    }
    
    
    public static void main(String[] args) {
        Cadena_de_Markov cadena = new Cadena_de_Markov(0,1,0,0.01); 
    }
    
}
