/**
 *
 * @author Yerson Ferney Porras Garcia
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Principal {
    
    Scanner leer = new Scanner(System.in);
    String cadena;
    int nCasos;
    
    public void inicia(){
        nCasos = leer.nextInt();
        for (int i = 0; i < nCasos; i++) {
            cadena=leer.next();
            operar(cadena,i+1);
        }        
    }
    
    public void operar(String cadenaS,int casoN){
        char[] cadena = cadenaS.toCharArray();
        boolean r=false;
        List salida = new ArrayList();        
        salida.add(cadena[0]);
        for (int i = 1; i < cadena.length; i++) {            
            if (cadena[i]>=(char)salida.get(0)) {
                salida.add(0,cadena[i]);
            }
            else{
                salida.add(cadena[i]);
            }        
        }
        System.out.print("Case #"+casoN+": ");
        for (int i = 0; i < salida.size(); i++) {
            System.out.print(salida.get(i));
        }
        System.out.println("");
    }
    
        
    public static void main(String[] args) {
        
        Principal obj = new Principal();
        obj.inicia();
        
    }
    
}
