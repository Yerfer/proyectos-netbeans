/**
 *
 * @author Yerson Porras
 */

import java.util.Scanner;

public class Principal {
    
    Scanner leer = new Scanner(System.in);
    private int T;
    private int caso;
    private int[] vectorDigitos = {0,1,2,3,4,5,6,7,8,9};
    private boolean[] vectorBoolean = new boolean[10];
    
    public void pedir(){
        this.T=leer.nextInt();
        for (int i = 0; i < T; i++) {
            this.caso=leer.nextInt();
            for (int j = 0; j < vectorBoolean.length; j++) {
                vectorBoolean[j]=false;
            }
            operar(this.caso, (i+1));
        }
    }
    
    public void operar(int casoX, int nCaso){        
        if(casoX == 0){
            System.out.print("Case #"+nCaso+": INSOMNIA\n");
        }
        else{            
            int rta = recurso(casoX,1);
            System.out.println("Case #"+nCaso+": "+rta);
        }
    }
    
    public int recurso(int casoX,int I){
        char[] casoCadena;
        casoCadena = Integer.toString(I*casoX).toCharArray();
        for (int i = 0; i < casoCadena.length; i++) {
            for (int j = 0; j <= vectorBoolean.length; j++) {
                    if (Integer.parseInt(""+casoCadena[i]) == vectorDigitos[j]) {
                        vectorBoolean[j]=true;
                        if (full(vectorBoolean)) {
                            return(I*casoX);
                        }
                        break;
                    }
            }
        }
        I++;
        return recurso(casoX,I);  
    }
    
    public boolean full(boolean[] vectorBoolean){
        boolean rta = true;
        for (int i = 0; i < vectorBoolean.length; i++) {
            if(!vectorBoolean[i]){
                rta=false;
                return rta;
            }
        }
        return rta;
    }
    
  
    public static void main(String[] args) {
        Principal obj = new Principal();
        obj.pedir();
    }
    
}
