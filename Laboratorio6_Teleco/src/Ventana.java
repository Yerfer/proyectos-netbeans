
import javax.swing.JFrame;
import org.jfree.chart.*;
import static org.jfree.chart.ChartFactory.createLineChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Ventana extends javax.swing.JFrame {

    JFreeChart grafica, grafica2, grafica3, grafica4;
    DefaultCategoryDataset Datos = new DefaultCategoryDataset();
    DefaultCategoryDataset DatosCorrelacion = new DefaultCategoryDataset();
    DefaultCategoryDataset DatosSignalTiempo = new DefaultCategoryDataset();
    DefaultCategoryDataset DatosSignalFFT = new DefaultCategoryDataset();
    DefaultCategoryDataset DatosSignal = new DefaultCategoryDataset();
    
    
    public Ventana() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        botonIniciar = new javax.swing.JButton();
        unillanos = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("INICIO");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        botonIniciar.setText("INICIAR");
        botonIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIniciarActionPerformed(evt);
            }
        });

        unillanos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/unillanos.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(unillanos, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(botonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(114, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(unillanos, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(botonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIniciarActionPerformed
        
        this.setVisible(false);
        
        Lab6 laboratorio = new Lab6();
        int n=5;  //Cantidad de flip flops.
        int nValores = (int)((Math.pow(2.0,n))-1);
        int[][] secuenciasAll = new int[n][nValores];
        int m = 8;  //cantidad de datos de la señal a generar. 2^8=256.
        int poten2 = (int)Math.pow(2, m);  //cantidad total de los datos de la señal, debe ser potencia de 2 para poder aplicar la fft.
        int relacion = (int)Math.pow(2, 4);  //Cantidad de secuencias que hay por cada dato de la señal. Relación. Tambn debe ser potencia de 2 para la fft. 2^4=16 veces.
        int[] signalTiempo = laboratorio.generarSignal(poten2, relacion);
        double[] signalFFT = laboratorio.fftReal(signalTiempo);      
        //System.out.println("M = "+n);
        for (int i = 0; i < n-1; i++) {
            //System.out.println("x = "+(i+1));
            secuenciasAll[i] = laboratorio.secuenciaM_n_x(n, i+1);
            //System.out.println("");
        }
        int[] signalEnsanchada = laboratorio.ensansharDes(signalTiempo, secuenciasAll[1]); //Se escoge qué secuencia se implementa. en este caso 2.
        int[] signalDesensanchada = laboratorio.ensansharDes(signalEnsanchada, secuenciasAll[1]);  //Se desensancha con la misma secuencia: la 2.
        int[] signalDesensanchadaOtra = laboratorio.ensansharDes(signalEnsanchada, secuenciasAll[3]); //Se desensancha con otra secuencia: la 4.
        double[] signalEnsanchadaFFT = laboratorio.fftReal(signalEnsanchada);              //=> las respectivas fft de las señales.
        double[] signalDesensanchadaFFT = laboratorio.fftReal(signalDesensanchada);      //=> las respectivas fft de las señales.
        double[] signalDesensanchadaOtraFFT = laboratorio.fftReal(signalDesensanchadaOtra);  //=> las respectivas fft de las señales.
        
        for (int i = 0; i < nValores; i++) {
            Datos.addValue(secuenciasAll[0][i], "X=1", i+"");
            Datos.addValue(secuenciasAll[1][i], "X=2", i+"");
            Datos.addValue(secuenciasAll[2][i], "X=3", i+"");
            Datos.addValue(secuenciasAll[3][i], "X=4", i+"");
        }
        
        grafica = ChartFactory.createBarChart3D("Generador de Secuencias tipo M [5, X]", "t", "Y", Datos, PlotOrientation.VERTICAL, true, true, false);
    
        ChartPanel Panel = new ChartPanel(grafica);
        JFrame ventanaSecuencias = new JFrame("Lab 6 - Teleco I");
        ventanaSecuencias.getContentPane().add(Panel);
        ventanaSecuencias.pack();
        ventanaSecuencias.setVisible(true);
        ventanaSecuencias.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        for (int i = 0; i < 4; i++) {
            DatosCorrelacion.addValue(laboratorio.correlacion(secuenciasAll[0], secuenciasAll[i]), "X=1", (i+1)+"");
            DatosCorrelacion.addValue(laboratorio.correlacion(secuenciasAll[1], secuenciasAll[i]), "X=2", (i+1)+"");
            DatosCorrelacion.addValue(laboratorio.correlacion(secuenciasAll[2], secuenciasAll[i]), "X=3", (i+1)+"");
            DatosCorrelacion.addValue(laboratorio.correlacion(secuenciasAll[3], secuenciasAll[i]), "X=4", (i+1)+"");
        }
        
        grafica2 = ChartFactory.createBarChart3D("Correlaciones", "X", "Y", DatosCorrelacion, PlotOrientation.VERTICAL, true, true, false);
    
        ChartPanel Panel2 = new ChartPanel(grafica2);
        JFrame ventanaCorrelaciones = new JFrame("Lab 6 - Teleco I");
        ventanaCorrelaciones.getContentPane().add(Panel2);
        ventanaCorrelaciones.pack();
        ventanaCorrelaciones.setVisible(true);
        ventanaCorrelaciones.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
                
        System.out.println("Tamaño de la SeñalFFT: "+signalFFT.length);
        //Gráficas en FRECUENCIAS.  
        for (int i = 0; i < signalFFT.length; i++) {            
            //if(i%100 == 0){  //Esta parte se ahbilita si quiere mostrar por bloques y no de forma continua.
                DatosSignalFFT.addValue(signalFFT[i], "Señal Original", i+"");
                DatosSignalFFT.addValue(signalEnsanchadaFFT[i], "Señal Ensanchada X=2", i+"");
                DatosSignalFFT.addValue(signalDesensanchadaFFT[i], "Señal Desensanchada X=2", i+"");
                DatosSignalFFT.addValue(signalDesensanchadaOtraFFT[i], "Señal Desensanchada Otra X=4", i+"");
            //}            
        }
        
        grafica3 = ChartFactory.createLineChart("Espectro de la Señal", "frecuencia (Hz)", "amplitud", DatosSignalFFT, PlotOrientation.VERTICAL, true, true, false);
    
        ChartPanel Panel3 = new ChartPanel(grafica3);
        JFrame ventanaFFT = new JFrame("Lab 6 - Teleco I");
        ventanaFFT.getContentPane().add(Panel3);
        ventanaFFT.pack();
        ventanaFFT.setVisible(true);
        ventanaFFT.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        System.out.println("Tamaño de la Señal: "+signalTiempo.length);
        //Gráficas en TIEMPO.        
        for (int i = 0; i < signalTiempo.length; i++) {            
            //if(i%100 == 0){  //Esta parte se ahbilita si quiere mostrar por bloques y no de forma continua.
                DatosSignal.addValue(signalTiempo[i], "Señal Original", i+"");
                DatosSignal.addValue(signalEnsanchada[i], "Señal Ensanchada X=2", i+"");
                DatosSignal.addValue(signalDesensanchada[i], "Señal Desensanchada X=2", i+"");
                DatosSignal.addValue(signalDesensanchadaOtra[i], "Señal Desensanchada Otra X=4", i+"");
            //}            
        }
        
        grafica4 = ChartFactory.createLineChart("Señal", "tiempo (t)", "data", DatosSignal, PlotOrientation.VERTICAL, true, true, false);
    
        ChartPanel Panel4 = new ChartPanel(grafica4);
        JFrame ventanaTiempo = new JFrame("Lab 6 - Teleco I");
        ventanaTiempo.getContentPane().add(Panel4);
        ventanaTiempo.pack();
        ventanaTiempo.setVisible(true);
        ventanaTiempo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }//GEN-LAST:event_botonIniciarActionPerformed

    
    
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonIniciar;
    private javax.swing.JLabel unillanos;
    // End of variables declaration//GEN-END:variables
}
