
import org.apache.commons.math3.transform.*;
import org.apache.commons.math3.transform.DftNormalization;

public class Lab6 {
    
    private int n=5;  //Cantidad de flip flops.
    private int nValores = (int)((Math.pow(2.0,n))-1);
    private double[] secuenciaM = new double[this.nValores];    
    private int[] q;// = new int[(int)n];
    private int[] d;// = new int[(int)n];
    
    
    public void secuenciaM_5_1(){
        resetQD();
        resetSecuenciaM();
        
        for (int i = 0; i < nValores; i++) {
            for (int ffI = 0; ffI < this.n; ffI++) {  //ffI es el i flip flip.

                if (ffI == 0) {
                    d[0]=xor(q[0],q[4]);
                }
                
                d[ffI+1]=q[ffI];
                if(d[ffI]==1){
                    if(q[ffI]==1){
                        q[ffI] = d[ffI];                                     
                    }else{                                                             
                        q[ffI] = 1;
                    }
                }
                else{
                    if(q[ffI]==1){
                        q[ffI] = d[ffI];                                     
                    }else{                                                        
                        q[ffI] = 0;
                    }
                }
            }
            
            this.secuenciaM[i] = q[q.length-1];
                        
        }
        
    }
    
    // n = cantidad de flips flops y x = al flip flop de se une al xor con el último.
    public int[] secuenciaM_n_x(int n, int x){
        resetQD(n);
        //resetSecuenciaM();
        
        int nValores = (int)((Math.pow(2.0,n))-1);
        //nValores = nValores*3;  //Solo para comprobar que se vuelve cíclico.
        
        int[] secuenciaM = new int[nValores];
        
        for (int i = 1; i < nValores-1; i++) { //Toca nValores -1 pq al resetear tds los flips al iniciar toma el primer valor de qn sin procesar más ff.
            
            if (i==1) {
                secuenciaM[0] = q[q.length-1];
            }
            
            for (int ffI = 0; ffI < n; ffI++) {  //ffI es el i flip flip.

                if (ffI == 0) {
                    d[0]=xor(q[x-1],q[n-1]);
                }
                if(ffI<n-1){
                    d[ffI+1]=q[ffI];
                }
                if(d[ffI]==1){
                    if(q[ffI]==1){
                        q[ffI] = d[ffI];                                     
                    }else{                                                             
                        q[ffI] = 1;
                    }
                }
                else{
                    if(q[ffI]==1){
                        q[ffI] = d[ffI];                                     
                    }else{                                                        
                        q[ffI] = 0;
                    }
                }
            }
            
            secuenciaM[i] = q[q.length-1];
                        
        }
        //imprimirRTA(secuenciaM);
        return secuenciaM;
    }
    
    public void resetQD(){
        this.q = new int[(int)this.n];
        this.d = new int[(int)this.n];
        for (int i = 0; i < this.n; i++) {            
                this.q[i]=1;
                this.d[i]=0;
        }
    }
    
    public void resetQD(int n){
        this.q = new int[(int)n];
        this.d = new int[(int)n];
        for (int i = 0; i < n; i++) {            
                this.q[i]=1;
                this.d[i]=0;
        }
    }
    
    public void imprimirRTA(){
        for (int i = 0; i < this.secuenciaM.length; i++) {
            System.out.print(this.secuenciaM[i]+" |");
        }
    }  
    
    public void imprimirRTA(int[] secuenciaM){
        for (int i = 0; i < secuenciaM.length; i++) {
            System.out.print(secuenciaM[i]+" | ");
        }
    } 
    
    public void resetSecuenciaM(){
        for (int i = 0; i < this.secuenciaM.length; i++) {
            this.secuenciaM[i]=0;
        }
    }
    
    public int xor(int x, int y){
        if (x+y == 1){
		return 1;
        }else{
		return 0;
        }
    }
    
    public int correlacion(int[] vectorX,int[] vectorY){
        int correlacion=0;        
        for (int i = 0; i < vectorX.length; i++) {
            correlacion = correlacion + (int)(vectorX[i] * vectorY[i]);
        }        
        return correlacion;
    }
    
    
    public int[] generarSignal(int poten2, int relacion){
        int[] signal = new int[poten2*relacion];
        int ale=0;
        //System.out.println("\nSeñal Generada (bits): ");
        for (int i = 0; i < signal.length; i++) {
            if(i%relacion == 0){
                ale = (int)(Math.random()*2);
            }            
            signal[i] = ale;
            //System.out.print(signal[i]+" | ");
        }        
        return signal;
    }
    
    public double[] fftReal(int[] vector){
        double[] fftReal = new double[vector.length];
        FastFourierTransformer obj = new FastFourierTransformer(DftNormalization.STANDARD);
        for (int i = 0; i < vector.length; i++) {
            fftReal[i] = obj.transform(conversionIntToDouble(vector), TransformType.FORWARD)[i].getReal();
        }
        //imprimirRTA(fftReal);
        return fftReal;
    }
    
    public double[] conversionIntToDouble(int[] vectorInt){
        double[] vectorDouble = new double[vectorInt.length];
        for (int i = 0; i < vectorDouble.length; i++) {
            vectorDouble[i] = vectorInt[i];
        }
        return vectorDouble;
    }
    
    public int[] ensansharDes(int[] signal, int[] secuenciaM){
        int[] signalEnsanchada = new int[signal.length];
        int contador = 0;
        for (int i = 0; i < signalEnsanchada.length; i++) {
            signalEnsanchada[i] = xor(signal[i], secuenciaM[contador++]);
            if( contador == (secuenciaM.length-1) ){
                contador = 0;
            }
        }
        return signalEnsanchada;
    }
    
    /*public static void main(String[] args) {
        Lab6 obj = new Lab6();
        int n = 5;
        int poten2 = (int)Math.pow(2, n);
        int relacion = (int)Math.pow(2, 8);
        obj.secuenciaM_n_x(5, 4);
        obj.fftReal(obj.generarSignal(poten2,relacion));
        
    }*/
    
}
